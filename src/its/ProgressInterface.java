package its;

import java.util.ArrayList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 *
 * @author Ryan Marker
 */
public class ProgressInterface implements NodeGraphics
{

    public enum State 
    {
        PASSED,
        INCORRECT,
        STANDARD,
        TESTING;
    }
    
    
    private final StackPane root = new StackPane();
    
    private ArrayList<ProgressGraph> graphs = new ArrayList<>();

    private int curGraph;
    
    public ProgressInterface()
    {
        graphs.add(new ProgressGraph());
        curGraph = 0;
        root.getChildren().add(graphs.get(0).getRoot());
    }
    
    public void setProblemGraph(int probNum)
    {
        while (graphs.size() <= probNum)
        {
            graphs.add(new ProgressGraph());
        }
        
        curGraph = probNum;
        
        root.getChildren().clear();
        root.getChildren().add(graphs.get(curGraph).getRoot());
    }
    
    public void addLine(int lineNum)
    {
        graphs.get(curGraph).addLine(lineNum);
    }
    
    /**
     * Set the state of a given step in the current problem graph.
     * 
     * @param seqNum The step/sequence number to set the state of.
     * @param state The new state.
     * @param force If false then if the state is already PASSED then it will not be changed.
     */
    public void setSeqState(int seqNum, State state, boolean force)
    {
        ProgressGraph graph = graphs.get(curGraph);
        
        // if passed then cannot change without force
        if (!force && graph.getState(seqNum) == State.PASSED)
            return;
        
        graph.setState(seqNum, state);
    }
    
    public void setSelectedStep(int seqNum)
    {
        ProgressGraph graph = graphs.get(curGraph);
        
        graph.setSelectedStep(seqNum);
    }
    
    public int getNextSeqNum()
    {
        return graphs.get(curGraph).getSequenceSize();
    }
    
    
    @Override
    public Node getRoot()
    {
        return root;
    }

    
    private class ProgressGraph implements NodeGraphics
    {
        //TODO: vertical lines
        private HBox root = new HBox();
        
        private ArrayList<Step> sequence = new ArrayList<>();
        private ArrayList<VBox> lines = new ArrayList<>();
        
        private int lastLine = 0;
        
        private int height = 0;
        
        private int curSelectedStep = 0;
        

        public ProgressGraph()
        {
            root.setPadding(new Insets(10));
        }

        
        public void addLine(int lineNum)
        {
            // add lines needed
            while (lines.size() < lineNum)
            {
                VBox vbox = new VBox(); 
                vbox.setAlignment(Pos.BOTTOM_CENTER);
                vbox.setSpacing(10);
                
                
                lines.add(vbox);
                root.getChildren().add(vbox);
            }
            
            // check if need to add line num text
            if (lines.get(lineNum-1).getChildren().size() == 0)
            {
                Text lineNumText = new Text(Integer.toString(lineNum));
                lineNumText.setFont(Font.font(14));
                
                lines.get(lineNum-1).getChildren().add(lineNumText);
            }
            
            
            // if line num is less than last line (went backwards) then increase height
            if (lineNum < lastLine)
            {
                height ++;
            }
            
            
            // make sure segment is at the correct height
            while (lines.get(lineNum-1).getChildren().size() < height)
            {
                // add dummy steps to get correct height
                Step dummy = new Step(-1);
                
                //lines.get(lineNum-1).getChildren().add(lines.get(lineNum-1).getChildren().size()-1, dummy.getRoot()); // place just before line num
                lines.get(lineNum-1).getChildren().add(0, dummy.getRoot()); // place just before line num
                dummy.getRoot().setVisible(false);
                dummy.setStatus(State.TESTING);
            }
            
            
            Step segment = new Step(sequence.size());
            sequence.add(segment);
            
            lines.get(lineNum-1).getChildren().add(0,segment.getRoot());
            
            
            // keep height updated for future
            if (height < lines.get(lineNum-1).getChildren().size() - 1)
            {
                height = lines.get(lineNum-1).getChildren().size() - 1;
            }
            
            
            lastLine = lineNum;
            
        }
        
        public void setState(int seqNum, State state)
        {
            sequence.get(seqNum).setStatus(state);
        }
        
        public State getState(int seqNum)
        {
            return sequence.get(seqNum).getStatus();
        }
        
        public void setSelectedStep(int seqNum)
        {
            if (seqNum < sequence.size())
            {
                sequence.get(curSelectedStep).setLineStrokeWidth(-1);
                sequence.get(seqNum).setLineStrokeWidth(6);
                curSelectedStep = seqNum;
            }
        }
        
        public int getSequenceSize()
        {
            return sequence.size();
        }
    
        @Override
        public Node getRoot()
        {
            return root;
        }
        
        
        
        private class Step implements NodeGraphics
        {

            private static final int LINE_WIDTH = 20;
            private static final int LINE_STROKE_WIDTH = 5;
            
            
            private int seqNum;
            private State state;
            
            private VBox root = new VBox();
            
            private Line line = new Line(0, 0, LINE_WIDTH, 0);

            public Step(int seqNum)
            {
                this.seqNum = seqNum;
                state = State.STANDARD;
                
                line.setStrokeWidth(LINE_STROKE_WIDTH);
                
                root.getChildren().addAll(line);
            }
            
            
            /**
             * Set the width of the line stroke. A negative value causes the default to be used.
             * 
             * @param width The width to use. Negative for the default.
             */
            public void setLineStrokeWidth(double width)
            {
                if (width < 0)
                {
                    line.setStrokeWidth(LINE_STROKE_WIDTH);
                }
                else
                {
                    line.setStrokeWidth(width);
                }
            }
            
            
            public void setStatus(State state)
            {
                this.state = state;
                
                switch (state)
                {
                    case INCORRECT:
                        line.setStroke(Paint.valueOf("red"));
                        break;
                        
                    case PASSED:
                        line.setStroke(Paint.valueOf("green"));
                        break;
                        
                    case STANDARD:
                        line.setStroke(Paint.valueOf("black"));
                        break;
                        
                    case TESTING:
                        line.setStroke(Paint.valueOf("orange"));
                        break;
                }
            }
            
            public State getStatus()
            {
                return state;
            }
            
            
            @Override
            public Node getRoot()
            {
                return root;
            }
            
        }
        
    }
    
}
