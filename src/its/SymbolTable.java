package its;

import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Separator;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;

/**
 *
 * @author Ryan Marker
 */
public class SymbolTable implements NodeGraphics
{   
    
    private final HBox hBox = new HBox();
    public final ToggleGroup group = new ToggleGroup();
    private final ToggleButton varButton = new ToggleButton("Variable");
    private final ToggleButton arrButton = new ToggleButton("Array");
    private final ToggleButton deleteButton = new ToggleButton("Delete");
    private final Button clearButton = new Button("Clear All");
    private final Button frameButton = new Button("Frame");
    private final Button resetHeapButton = new Button("Reset Heap");
    private final Button constraintBtn = new Button("Request feedback");
    private final ToggleButton pointerButton = new ToggleButton("Pointer");
    private final Button solButton = new Button("Show Solution");
    
    private final Separator sep = new Separator(Orientation.VERTICAL);
    private final Separator sep2 = new Separator(Orientation.VERTICAL);
    

    public SymbolTable() 
    {
    	
        varButton.setToggleGroup(group);
        arrButton.setToggleGroup(group);
        deleteButton.setToggleGroup(group);
        pointerButton.setToggleGroup(group);
        
        hBox.setPadding(new Insets(10));
        hBox.setSpacing(10);
        hBox.getChildren().addAll(varButton, arrButton, deleteButton, pointerButton, sep, frameButton, resetHeapButton, clearButton, sep2, constraintBtn, solButton);
        
        varButton.setTooltip(new Tooltip("When selected, clicking in the stack or heap will append a new variable at the bottom"));
        arrButton.setTooltip(new Tooltip("When selected, clicking in the stack or heap will append a new array at the bottom"));
        deleteButton.setTooltip(new Tooltip("When selected, clicking on an array or variable will remove it"));
        pointerButton.setTooltip(new Tooltip("When selected, right clicking a variable will store a reference to it and left clicking a variable will replace its value with the reference currently stored"));
        
        frameButton.setTooltip(new Tooltip("Create a new stack frame which all subsequent objects on the stack will be placed in"));
        frameButton.setOnAction((ActionEvent ae) -> {
            ITS.stack.addFrame();
        });
        
        resetHeapButton.setTooltip(new Tooltip("Sorts all of the objects on the heap by number (name)."
                + " It then reduces all the names of the objects on the heap to the smallest possible value and resets the next heap name generated for the next object"));
        resetHeapButton.setOnAction((ActionEvent ae) -> {
            ITS.heap.reset();
        });
        
        clearButton.setTooltip(new Tooltip("Remove everything in both the stack and heap"));
        clearButton.setOnAction((ActionEvent ae) -> {
            ITS.stack.clear();
            ITS.heap.clear();
            
            ITS.printLogMsg("Claer solution");
        });
        
        constraintBtn.setTooltip(new Tooltip("Get feedback on the current solution attempt"));
        constraintBtn.setOnAction((ActionEvent ae) -> {
            ITS.giveFeedback();
        });
        
        solButton.setTooltip(new Tooltip("Bring up a new window showing the current solution. This window will automatically update as the problem is stepped through."));
        solButton.setOnAction((ActionEvent ae) -> {
            ITS.showSolution();
        });
    }
    
    
    @Override
    public Node getRoot() 
    {
        return hBox;
    }
    
}
