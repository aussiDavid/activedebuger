package its;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

/**
 *
 * @author Ryan Marker
 */
public class Heap implements MemoryContainer
{

    private int nextNumber = 0;
    
    public int getNextNumber()
    {
        
        int result = nextNumber;
        nextNumber ++;
        return result;
    }
    
    
    private final ScrollPane pane = new ScrollPane();
    
    private final VBox vBox = new VBox();
    private final VBox memBox = new VBox();
    
    private final ArrayList<MemoryObject> memObjects = new ArrayList<>();
    
    private boolean isLocked = false;
    
    private boolean logged = false;
    
    
    public Heap(boolean log)
    {
        logged = log;
        
        Label title = new Label("Heap");
        title.setAlignment(Pos.CENTER);
        title.setFont(new Font(20));
        
        vBox.setAlignment(Pos.CENTER);
        vBox.getChildren().add(title);
        
        vBox.getChildren().add(memBox);
        
        pane.setContent(vBox);
        pane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        pane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        pane.setPannable(false);
        pane.setFitToWidth(true);
        memBox.setSpacing(10);
        
        pane.setOnMousePressed((MouseEvent me) -> {
            if (isLocked)
                return;
            
            ToggleButton selected = (ToggleButton) ITS.symbolTable.group.getSelectedToggle();
            
            if (selected == null)
                return;
            
            MemoryObject newObj = null;
            
            if (selected.getText().equalsIgnoreCase("variable"))
                newObj = new Var(false, logged);
            else if (selected.getText().equalsIgnoreCase("array"))
            {
                newObj = new Arr(logged);
            }
            
            if (newObj != null)
                addMemObj(newObj);
        });
    }
    
    @Override
    public void addMemObj(MemoryObject obj)
    {
        memObjects.add(obj);
        obj.setName(Integer.toString(getNextNumber()));
        obj.setFrame(-1);
        memBox.getChildren().add(obj.getRoot());
    }

    @Override
    public boolean removeMemObj(MemoryObject obj)
    {
        int index = memObjects.indexOf(obj);
        
        if (index >= 0)
        {
            memBox.getChildren().remove(memBox.getChildren().size() - memObjects.size() + index);
            memObjects.remove(index);
            return true;
        }
        
        return false;
    }

    @Override
    public MemoryObject get(String name)
    {
        for (MemoryObject m : memObjects)
        {
            if (m.getName().equals(name))
                return m;
        }
        
        return null;
    }
    
    @Override
    public Collection<MemoryObject> get()
    {
        return new LinkedList<>(memObjects);
    }
    
    /**
     * Sort all the memory objects on the heap. Then change their names to start with zero and increment for each to mimic adding the objects from the start.
     */
    public void reset()
    {
        memObjects.sort((MemoryObject m1, MemoryObject m2) -> {
            String n1 = m1.getName();
            String n2 = m2.getName();
            
            int i1 = -1;
            int i2 = -1;
            
            try
            {
                i1 = Integer.parseInt(n1);
            }
            catch (NumberFormatException ex)
            {
                return 1;
            }
            
            try
            {
                i2 = Integer.parseInt(n2);
            }
            catch (NumberFormatException ex)
            {
                return -1;
            }
            
            return i1 - i2;
        });
        
        memBox.getChildren().remove(memBox.getChildren().size() - memObjects.size(), memBox.getChildren().size());
        
        int next = 0;
        
        for (MemoryObject m : memObjects)
        {
            try
            {
               int num = Integer.parseInt(m.getName());
               
               m.setName(Integer.toString(next));
               next ++;
            }
            catch (NumberFormatException ex)
            {
                
            }
            
            memBox.getChildren().add(m.getRoot());
        }
        
       nextNumber = next;
    }

    @Override
    public void clear()
    {
        nextNumber = 0;
        
        while (!memObjects.isEmpty())
        {
            removeMemObj(memObjects.get(0));
        }
    }

    @Override
    public void setLocked(boolean l)
    {
        isLocked = l;
        
        vBox.setMouseTransparent(l);
        
        for (MemoryObject m : memObjects)
        {
            m.setLocked(l);
        }
    }
    
    
    @Override
    public String generateTextRep()
    {
        String result = "";
        
        for (MemoryObject v : memObjects)
        {
            result += v.generateTextRep();
        }
        
        return result;
    }
    
    
    @Override
    public Node getRoot()
    {
        return pane;
    }
    
}
