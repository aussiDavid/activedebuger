package its;

import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.CubicCurve;
import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.Line;
import javafx.scene.shape.Path;

/**
 *
 * @author Ryan Marker
 */
public class Memory implements NodeGraphics
{

    StackPane stack = new StackPane();
    
    SplitPane split = new SplitPane();
    
    Pane arrowPane = new Pane();

    
    public Memory()
    {
        arrowPane.setMouseTransparent(true);
        
        stack.setAlignment(Pos.TOP_LEFT);
        stack.getChildren().addAll(split, arrowPane);
    }
    
    public void addMemContainer(MemoryContainer memCon)
    {
        split.getItems().add(memCon.getRoot());
    }
    
    public void addArrow(MemoryObject m1, MemoryObject m2)
    {
        
        Point2D p1, p2;
        
        
        p1 = m1.getRoot().localToScene(m1.getArrowX(), m1.getArrowY());
        p2 = m2.getRoot().localToScene(m2.getArrowX(), m2.getArrowY());
        
        
        p1 = arrowPane.sceneToLocal(p1);
        p2 = arrowPane.sceneToLocal(p2);
        
        double x1,y1,x2,y2;
        
        x1 = p1.getX();
        y1 = p1.getY();
        
        x2 = p2.getX();
        y2 = p2.getY();
        
        
        double dx = x2 - x1;
        double dy = y2 - y1;
        
        
        double controlx = split.getWidth()/2; // center of memory in x
        double controly = y1 + 0.5*dy;

        
        //QuadCurve arrow = new QuadCurve(x1, y1, controlx, controly, x2, y2);
        //Line arrow = new Line(x1, y1, x2, y2);
        CubicCurve arrow = new CubicCurve(x1, y1, controlx, y1, controlx, y2, x2, y2);
        
        arrowPane.getChildren().add(arrow);
    }
    
    public void clearArrows()
    {
        arrowPane.getChildren().clear();
    }
    
    
    @Override
    public Node getRoot()
    {
        return stack;
    }
    
}
