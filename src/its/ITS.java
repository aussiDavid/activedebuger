package its;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.Optional;
import java.util.Scanner;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

/**
 *
 * @author Ryan Marker
 */
public class ITS extends Application
{
	
	//Things Tom has added
	public static ArrayList<ArrayList<String > > lineFeedback = null;
	public static ArrayList<String> activeErrors = new ArrayList<String>();
	public static BindingManager bindingManager = new BindingManager();
	
    @Override
    public void start(Stage primaryStage)
    {
        IDAlert.setTitle("Enter ID");
        IDAlert.setHeaderText(null);
        IDAlert.setContentText("Please enter your ID:");

        // get user id
        // Note must be before primaryStage.show() or else stuffs up primary stage
        getAndSetID();

        // set up solution window
        SplitPane solRoot = new SplitPane(solStack.getRoot(), solHeap.getRoot());
        Scene solScene = new Scene(solRoot, 500, 500);
        solWindow.setTitle("Solution");
        solWindow.initModality(Modality.NONE);
        solWindow.initOwner(primaryStage);
        solWindow.setScene(solScene);
        solWindow.setOnCloseRequest((WindowEvent e) -> {ITS.printLogMsg("Hide solution");});

        ram.addMemContainer(stack);
        ram.addMemContainer(heap);
        
        VBox feedbackAndNav = new VBox();
        feedbackAndNav.getChildren().add(feedback.getRoot());

        BorderPane root = new BorderPane();
        root.setBottom(symbolTable.getRoot());
        root.setCenter(ram.getRoot());
        root.setTop(feedbackAndNav);
        root.setLeft(probInterface.getRoot());
//        root.setLeft(progress.getRoot());
        root.setBottom(progress.getRoot());
        
        probInterface.setProblemNum(0);

        // do after the primary stage is set up
        Platform.runLater(() -> {displayHelp();});


        Scene scene = new Scene(root, 800, 500);

        primaryStage.setTitle("Active Debugger");
        primaryStage.setScene(scene);
        primaryStage.setMaximized(true);
        primaryStage.show();


    }

    @Override
    public void stop() throws Exception
    {
        super.stop();

        printLogMsg("Exit application");
    }



    public static final FeedbackInterface feedback = new FeedbackInterface();
    public static final ProblemInterface probInterface = new ProblemInterface();
    public static final SymbolTable symbolTable = new SymbolTable();
    public static final ProgressInterface progress = new ProgressInterface();
    public static final Memory ram = new Memory();
    public static final Stack stack = new Stack(true);
    public static final Heap heap = new Heap(true);

    public static final Stage solWindow = new Stage(StageStyle.UTILITY);
    public static final Stack solStack = new Stack(false);
    public static final Heap solHeap = new Heap(false);

    public static Solution ss;
    public static Solution is;

    public static final ArrayList<Problem> problems = new ArrayList<>();
    public static final ArrayList<Constraint> constraints = new ArrayList<>();

    private static int submissionID = (int)(Math.random()*Integer.MAX_VALUE/2); // initilise random stating id for student model submissions

    public static final TextInputDialog IDAlert = new TextInputDialog("axxxxxxx");

    //private static final Stage helpWindow = new Stage(StageStyle.UTILITY);


    public static void getAndSetID()
    {

        Optional<String> result = IDAlert.showAndWait();

        // crashes with media player in help window
        /*
        if (!result.isPresent())
        {
            Platform.runLater(() -> {Platform.exit();});
        }
        */

        // set the entered user ID
        result.ifPresent(id -> {
            probInterface.userID.setText(id);
            printLogMsg("Set ID: " + id);
        });



        // clear student model
        studentModel.clear();
    }

    public static void displayHelp()
    {
        feedback.displayIntro();

        //helpWindow.show();
    }

    /**
     * Get the textual representation of the current student solution.
     * @return The representation as a String
     */
    public static String generateTextRep()
    {
        String result = "Var: @";

        result += stack.generateTextRep();

        result += heap.generateTextRep();

        return result;
    }
   

    private static void checkConstraints(Solution ss, Solution is)
    {
        LinkedList<String> result = new LinkedList<>();
        
        
        for(int constraintIndex = 0 ; constraintIndex < constraints.size(); constraintIndex++)
        {
            Constraint con = constraints.get(constraintIndex);
            // get all bindings that feedback need to be generated for
            LinkedList<Binding> bindings = con.check(ss, is);
            

            // get feedback string for each binding
            for (Binding b : bindings)
            {

                String s = "";
                s += "- ";
                
                Binding temporaryContainer = ITS.bindingManager.alreadyStored(b);
                if(temporaryContainer==null)
                {	//If this binding has already been created before
                	ITS.bindingManager.addBinding(b);
                	b.timesSeenAlready++;
                }
                else
                {	//If this is the first time the binding has been created
                	System.out.println("Already present!");
                	b = temporaryContainer;
                	if(b.timesSeenAlready<=2)
                	{
                    	b.timesSeenAlready++;                		
                	}

                }
                
                String f = con.getMultilevelFeedback(b);
              
                	s += f + "\n\n";

                    result.add(s);
               
                
            }
            
        }
        

        // if no feedback then passed line
        if (result.isEmpty())
        {
            // set line of current problem to passed
            problems.get(probInterface.getProblemNum()).setPassedLine(CInterpreter.getCurLine(), true);
            probInterface.updateDisplay();

            // update progress display
            progress.setSeqState(CInterpreter.getStepNum(), ProgressInterface.State.PASSED, false);
        }
        else
        {
            // update progress display
            progress.setSeqState(CInterpreter.getStepNum(), ProgressInterface.State.INCORRECT, false);
        }

        // display feedback
        feedback.setFeedback(result);

        // increment submission id for student model
        submissionID ++;
    }

    public static void giveFeedback()
    {
        // set the ss and is solutions to be used when checking constraints
    	System.out.println("GIVING FEEDBACK!");
        Solution ss = new Solution(generateTextRep());

        Solution is;

        is = CInterpreter.getSolution();

        ITS.ss = ss;
        ITS.is = is;


        checkConstraints(ss, is);
    }

    /**
     * Make the solution stack and heap that are used for display contain the current ideal solution (is).
     */
    public static void updateIS()
    {
        // maybe make so this is only done when the solution window is displayed for better performance


        Solution sol = CInterpreter.getSolution();

        LinkedList<String> var = sol.getAllVar();

        solStack.clear();
        solHeap.clear();

        for (String v : var)
        {
            String location = v.substring(0, v.indexOf('.'));

            // remove location from var name
            v = v.substring(v.indexOf('.') + 1);


            String[] elements = v.split(" ");

            String name = elements[0];


            if (name.contains("["))
            {
                // part of array

                String aName = name.substring(0, name.indexOf('['));



                if (elements[1].equals("true"))
                {
                    Arr array = (Arr)solStack.get(aName);

                    if (array == null)
                    {
                        array = new Arr(false);

                        array.setName(aName);
                        array.setType(elements[2]);
                        array.setPointer(Boolean.parseBoolean(elements[3]));


                        solStack.addMemObj(array, Integer.parseInt(location.substring(5))); // ignore Frame part of location
                    }

                    int index = Integer.parseInt(name.substring(name.indexOf('[')+1, name.indexOf(']')));


                    if (array.getSize() <= index)
                        array.setSize(index+1);

                    array.setValue(index, elements[4]);

                }
                else
                {
                    Arr array = (Arr)solHeap.get(aName);

                    if (array == null)
                    {
                        array = new Arr(false);

                        // before setting details of array because addMemObj within heap overwrites name with current heap position
                        solHeap.addMemObj(array);

                        array.setName(aName);
                        array.setType(elements[2]);
                        array.setPointer(Boolean.parseBoolean(elements[3]));

                    }

                    int index = Integer.parseInt(name.substring(name.indexOf('[')+1, name.indexOf(']')));

                    if (array.getSize() <= index)
                        array.setSize(index+1);

                    array.setValue(index, elements[4]);
                }
            }
            else
            {
                MemoryObject memObj = new Var(v, false);

                if (elements[1].equals("true"))
                {
                    solStack.addMemObj(memObj, Integer.parseInt(location.substring(5))); // ignore Frame part of location
                }
                else
                {
                    solHeap.addMemObj(memObj);

                    memObj.setName(name);
                }
            }
        }
    }

    public static void showSolution()
    {
        printLogMsg("Show solution");

        updateIS();


        // make not respond to events so cannot edit solution (needed here as setLocked currentely does not affect new memory objects added so needs to update for any new additions

        solStack.setLocked(true);
        solHeap.setLocked(true);

        solWindow.show();

    }

    public static void hideSolution()
    {
        printLogMsg("Hide solution");

        solWindow.close();
    }

    public static void printLogMsg(String msg)
    {
        String id = probInterface.userID.getText();

        if (id.isEmpty() || msg.isEmpty())
            return;

        File dir = new File("log");

        if (!dir.isDirectory())
        {
            System.err.println("log directory does not exist");
            return;
        }

        File f = new File("log/" + id + ".log");

        // check that a log file for this user has already been created
        if (!f.isFile())
        {
            try
            {
                f.createNewFile();
            }
            catch (IOException ex)
            {
                System.err.println("IOException: " + ex.toString());
            }
        }

        try (FileWriter out = new FileWriter(f, true))
        {
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            out.write(dateFormat.format(new Date()) + ", " + generateTextRep() + ", " + msg + "\r\n"); // write log message
        }
        catch (IOException ex)
        {
            System.err.println(ex.toString());
        }
    }


    private static final ArrayList<String> studentModel = new ArrayList<>();

    /**
     * Adds an entry to the student model of the current user
     *
     * @param con The constraint that the entry refers to
     * @param probNum The problem number that the entry refers to
     * @param lineNum The line number of the corresponding problem that the entry refers to
     * @param passed Whether the constraint was passed or not
     */
    public static void addStudentModel(Constraint con, int probNum, int lineNum, boolean passed)
    {
        int conID = con.getId();

        // generate the message to add
        String msg = Integer.toString(submissionID) + ", " + Integer.toString(conID) + ", " + Integer.toString(probNum) + ", " + Integer.toString(lineNum) + ", " + Boolean.toString(passed);

        studentModel.add(msg);


        String id = probInterface.userID.getText();

        if (id.isEmpty() || msg.isEmpty())
            return;

        File dir = new File("log");

        if (!dir.isDirectory())
        {
            System.err.println("log dir does not exist");
            return;
        }

        File f = new File("log/" + id + ".model");

        if (!f.isFile())
        {
            try
            {
                f.createNewFile();
            }
            catch (IOException ex)
            {
                System.err.println("prev: " + ex.toString());
            }
        }

        try (FileWriter out = new FileWriter(f, true))
        {
            out.write(msg + "\r\n");
        }
        catch (IOException ex)
        {
            System.err.println(ex.toString());
        }
    }

    public static void printStudentModel()
    {
        for (String s : studentModel)
            System.out.println(s);
    }

    /**
     * Calculates results from the student model
     * @param con The id of the condition to take into account. If negative then checks all.
     * @param prob The number of the problem to check (starts at 0). If negative then all are included.
     * @param line The line number to check (starts at 1). If negative then all are included.
     * @param pass What result to calculate. 0 - number of fail, 1 - number of pass, 2 - total, 3 - percent passed
     * @return
     */
    public static double getStudentModel(int con, int prob, int line, int pass)
    {
        int passNum = 0;
        int failNum = 0;

        for (String s : studentModel)
        {
            String[] col = s.split(",");

            if (con < 0 || Integer.parseInt(col[0].trim()) == con)
            {
                if (prob < 0 || Integer.parseInt(col[1].trim()) == prob)
                {
                    if (line < 0 || Integer.parseInt(col[2].trim()) == line)
                    {
                        boolean p = Boolean.parseBoolean(col[3].trim());

                        if (p)
                            passNum++;
                        else
                            failNum++;
                    }
                }
            }
        }

        switch (pass)
        {
            case 0:
                return failNum;

            case 1:
                return passNum;

            case 2:
                return failNum + passNum;


            case 3:
                return passNum / (failNum + passNum);

        }

        return -1;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {

        // constraints

        File constraintsFile;

        constraintsFile = new File("constraints.txt");

        try (Scanner in = new Scanner(new BufferedReader(new FileReader(constraintsFile))))
        {
            while (in.hasNextLine())
            {
                String line = in.nextLine();

                if (line.isEmpty() || line.startsWith("/"))
                {
                    continue;
                }

                Constraint c = new Constraint(line);

                constraints.add(c);
            }
        }
        catch (FileNotFoundException ex)
        {
            System.err.println(ex.toString());
        }

        constraints.sort((Constraint c1, Constraint c2) -> {return (c2.getId() - c1.getId());});

        // problems	- TODO
        File folder;

        folder = new File("problems");

        File[] files = folder.listFiles();

        Arrays.sort(files, (File o1, File o2) -> {
            return o1.getName().compareTo(o2.getName());
        });

        Problem p;

        for (int i=0; i<files.length; i++)
        {
            File f = files[i];
            if (f.isDirectory())
                continue;

            p = new Problem(f.getPath());
            problems.add(p);
        }
        
        launch(args);
    }
}
