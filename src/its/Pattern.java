package its;

import java.util.LinkedList;
import java.util.ListIterator;

/**
 *
 * @author Ryan Marker
 */
public class Pattern
{
    
    private String[] terms;
    
    
    public Pattern(String s)
    {
        boolean insideQuote = false;
        
        // go through s and replace any ' ' inside " ". Eg "one two three" goes to "one" "two" "three" 
        for (int i=0; i<s.length(); i++)
        {
            if (s.charAt(i) == '"')
            {
                insideQuote = !insideQuote;
            }
            else if (s.charAt(i) == ' ' && insideQuote)
            {
                s = s.substring(0, i) + "\" \"" + s.substring(i+1, s.length());
                i--;
            }
        }
        
        // remove empty quotes
        s = s.replaceAll("\"\"", "");
        
        // remove duplicate consecutive spaces
        for (int i=0; i<s.length()-1; i++)
        {
            if (s.charAt(i) == ' ' && s.charAt(i+1) == ' ')
            {
                s = s.substring(0, i) + s.substring(i+1, s.length());
                i--;
            }
        }
        
        terms = s.split(" ");
        
    }
    
    public int getNumTerms()
    {
        return terms.length;
    }
    
    public String getTerm(int index)
    {
        return terms[index];
    }
    
    public boolean isLiteral(int index)
    {
        String term = terms[index];
        
        if (term.startsWith("\"") && term.endsWith("\""))
        {
            return true;
        }
        
        return false;
    }
    
    public boolean isWildcard(int index)
    {
        String term = terms[index];
        
        if (term.startsWith("?*"))
        {
            return true;
        }
        
        return false;
    }
    
    public boolean isWildcard(int index, Binding b)
    {
        String term = terms[index];
        
        if (term.startsWith("?*"))
        {
            if (term.length() == 2)
                return true;
            else
            {
                // named wildcard, check if allready defined for binding
                if (b.isDefined(term.substring(2, term.length())))
                    return false;
                else
                    return true;
            }
        }
        
        return false;
    }
    
    /**
     * checks if a string will pass a term of the pattern.
     * 
     * @param source The string to check the pattern against starting with the first character
     * @param index The term of the pattern to check
     * @param b The binding which defines relevant values
     * @param newBindings A list of new bindings produced which can be followed up with and which are successful.
     * @return Index of the next character in the string which was not needed to successfully match the pattern. 0 if the pattern failed. 
     */
    public int matchTerm(String source, int index, Binding b, LinkedList<Binding> newBindings)
    {
        // Note: with current setup newBindings doesn't need to be a list as won't have any more than one new binding
        
        String term = terms[index];
        
        // if term is literal
        if (term.charAt(0) == '"')
        {

            String searchString = term.substring(1, term.length()-1);
            
            return matchLiteral(source, searchString, b, newBindings);
            
        }
        else if (term.startsWith("?")) // term is parameter
        {
            String varName;
            
            if (term.charAt(1) == '*') // named wildcard in form ?*xxxxxxxxxx
                varName = term.substring(2, term.length());
            else // normal parameter with form ?xxxxxxxxx
                varName = term.substring(1,term.length());
                
            // if already defined value for this param in binding
            if (b.isDefined(varName))
            {
                String checkString = b.getValue(varName);
                
                return matchLiteral(source, checkString, b, newBindings);
                
            }
            else
            {
                // uninitilised param
                
                int startIndex = 0;
                
                // skip spaces
                while (startIndex < source.length() && source.charAt(startIndex) == ' ')
                {
                    startIndex ++;
                }
                
                // get index of next space
                int newIndex = source.indexOf(' ', startIndex);
                
                // no more spaces so set newIndex to rest of string
                if (newIndex == -1)
                {
                    newIndex = source.length();
                }
                
                // get value to set paramater with
                String newValue = source.substring(startIndex, newIndex);
                // get copy of binding
                Binding newBinding = new Binding(b);
                // add value for parameter
                newBinding.setValue(varName, newValue);
                newBindings.add(newBinding);
                
                return newIndex + 1;
            }
        }
        
        return 0;
    }
    
    /**
     * Checks if a literal will pass with a given source, and if it does then the binding is added to the set of new bindings which can proceed through the process.
     * 
     * @param source The string to check the literal against starting with the first character
     * @param searchString The string to check that the source starts with
     * @param b The binding which defines relevant values
     * @param newBindings A list of new bindings produced which can be followed up with and which are successful.
     * @return Index of the next character in the string which was not needed to successfully match the literal. 0 if the literal could not be matched. 
     */
    private static int matchLiteral(String source, String searchString, Binding b, LinkedList<Binding> newBindings)
    {
        // source not long enough so fail
        if (searchString.length() > source.length())
            return 0;


        if (source.substring(0, searchString.length()).equals(searchString))
        {
            // lieral matched so success

            // new binding is just the same binding passed with no alterations
            newBindings.add(b);

            // next character is at end of search string
            return searchString.length();
        }
        else
        {
            // not equal so fail
            return 0;
        }
    }

    @Override
    public String toString()
    {
        String result = "";
        
        for (String term : terms)
        {
            result += term + " ";
        }
        
        return result;
    }
     
    /**
     * Entry point to pattern matcher. Takes in a source string, a pattern and initial bindings. It them outputs a new set of bindings.
     * 
     * @param source source string
     * @param p the pattern to use
     * @param bindings the initial bindings
     * @return The new bindings created by the pattern matcher
     */
    public static LinkedList<Binding> patternMatch(String source, Pattern p, LinkedList<Binding> bindings)
    {
        source = source.trim();
        
        LinkedList<Binding> newBindings = new LinkedList<>();
        
        // for each binding
        for (ListIterator it = bindings.listIterator(); it.hasNext();)
        {
            Binding b = new Binding((Binding) it.next());
            
            LinkedList<Binding> adding;
            
            // preform a pattern match for the single binding starting at the beginning of the pattern and get the output
            adding = patternMatch(source, p, 0, b);
            
            if (adding.isEmpty())
            {
                // no output so binding failed. Set success as such and add to output
                b.setSuccess(false);
                
                newBindings.add(b);
            }
            else
            {
                newBindings.addAll(adding);
            }
        }
        
        Binding.clearRepeates(newBindings);
        
        return newBindings;
    }
    
    /**
     * Handles a wildcard term in a pattern. Either an unnamed wildcard or an undefined named wildcard.
     * 
     * @param source The source string
     * @param p The pattern
     * @param index The index of the term of the pattern which has the wildcard
     * @param b The initial binding to use
     * @return A list of the new bindings produced
     */
    private static LinkedList<Binding> patternWildCard(String source, Pattern p, int index, Binding b)
    {

        LinkedList<Binding> newBinding = new LinkedList<>();

        // index in source
        int i = 0;

        String term = p.getTerm(index);
        
        // check all possible values for wildcard
        while (i <= source.length())
        {
            // skip spaces and characters that don't come just after a space
            if (i != source.length() && (source.charAt(i) == ' ' || (i > 0 && source.charAt(i-1) != ' ')))
            {
                i++;
                continue;
            }
                
            // create copy
            Binding bcopy = new Binding(b);

            // named wildcard so add definition to copy
            if (term.length() > 2)
            {
                bcopy.setValue(term.substring(2, term.length()), source.substring(0, i));
            }

            String newString;
            
            // get everything from index
            if (i == source.length())
            {
                newString = "";
            }
            else
            {
                newString = source.substring(i);
            }

            // start pattern match on next term in pattern. In affect every thing from the start of the string to i is consumed by this wildcard.
            // If this option enables the pattern to finish successfully then will get new bindings as output otherwise empty list will be returned
            newBinding.addAll(patternMatch(newString, p, index+1, bcopy));
            
            i++;
        }

        // return all new bindings returned from all possible ways of setting wildcard
        return newBinding;
    }
    
    /**
     * Handles matching a pattern from a given index for a particular binding.
     * 
     * @param source The source string
     * @param p The pattern
     * @param index The index of the term of the pattern to start from
     * @param b The binding to use.
     * @return A list of the new bindings produced.
     */
    private static LinkedList<Binding> patternMatch(String source, Pattern p, int index, Binding b)
    {
        LinkedList<Binding> result = new LinkedList<>();
        
        
        // finished pattern
        if (index >= p.getNumTerms())
        {
            // finish source - success
            if (source.length() == 0)
            {
                // return current binding as a success
                result.add(b);
                return result;
            }
            else
            {
                return result;
            }
        }
        
        
        if (p.isWildcard(index, b))
        {
            result.addAll(patternWildCard(source, p, index, b));
            return result;
        }
        
        // finished source and isn't wildcard - fail
        if (source.length() == 0)
        {
            return result;
        }
        
        LinkedList<Binding> newBindings = new LinkedList<>();
        
        // perform the match for the given term and get where reached in the source string
        int newIndex = p.matchTerm(source, index, b, newBindings);
        
        // if didn't fail
        if (newIndex != 0)
        {
            String newString;
            
            // get part of string that is left skipping spaces
            if (newIndex > source.length())
                newString = "";
            else
            {
                while (newIndex < source.length() && source.charAt(newIndex) == ' ')
                    newIndex ++;
                
                newString = source.substring(newIndex);
            }
                
            // for all of the new bindings produced from the term of the pattern, get the results from the next term
            for (Binding bind : newBindings)
                result.addAll(patternMatch(newString, p, index+1, bind));
            
            return result;
        }
        
        return result;
    }
    
    private static Solution getSolution(String s)
    {
        
        // temp
        
        if (s.equalsIgnoreCase("ss"))
            return ITS.ss;
        else if (s.equalsIgnoreCase("is"))
            return ITS.is;
        else if (s.equalsIgnoreCase("is-"))
        {
            int oldStep = CInterpreter.getStepNum();
            
            int numNext = 0;
            
            CInterpreter.start();
            
            while (CInterpreter.getStepNum()< oldStep)
            {
                CInterpreter.next();
                numNext ++;
            }
            
            CInterpreter.start();
            
            for (int i=0; i<numNext-1; i++)
            {
                CInterpreter.next();
            }
            
            Solution sol =  CInterpreter.getSolution();
            
            CInterpreter.start();
            while (CInterpreter.getStepNum() < oldStep)
                CInterpreter.next();
            
            return sol;
        }
           
        
        
        return null;
    }
    
    /**
     * Perform a match operation which is a wrapper which makes use of the pattern matcher.
     * 
     * @param params The parameters for the match operation as a string. Format: "solution component (pattern)" eg "ss var (?n)"
     * @param bindings The input bindings
     * @return The output bindings from the operation
     */
    public static LinkedList<Binding> match(String params, LinkedList<Binding> bindings)
    {
        // seperate params
        String[] terms = params.split(" ", 3);
        
        Solution sol = getSolution(terms[0]);
        
        String source = sol.toString(terms[1]);
        
        // remove () around pattern, maybe move to constructor
        terms[2] = terms[2].substring(1, terms[2].length()-1);
        
        Pattern pat = new Pattern(terms[2]);
        
        // call pattern matcher
        return patternMatch(source, pat, bindings);

    }
    
    /**
     * Perform a test operation which is a wrapper which makes use of the pattern matcher. Allows the value of an already defined parameter to be tested.
     * 
     * @param params The parameters for the test operation as a string. Format: "solution (source pattern)" eg "ss (and ?n)"
     * @param bindings The input bindings
     * @return The output bindings from the operation
     */
    public static LinkedList<Binding> test(String params, LinkedList<Binding> bindings)
    {
        // separate params
        String[] terms = params.split(" ", 2);
        
        Solution sol = getSolution(terms[0]);
        
        // remove () around pattern, maybe move to constructor
        terms[1] = terms[1].substring(1, terms[1].length()-1);
        
        String[] terms2 = terms[1].split(" ", 2);
        
        Pattern pat = new Pattern(terms2[1]);
        
        // call pattern matcher
        return patternMatch(terms2[0], pat, bindings);
    }
    
    /**
     * Perform a testSymbol operation which is a wrapper which makes use of the pattern matcher. Allows a pattern to be matched to the individual characters of an already defined parameter. If the parameter given has not been defined than it fails.
     * 
     * @param params The parameters for the test operation as a string. Format: "solution parameter (pattern)" eg "ss ?n (\"\'\" ?* \"\'\")"
     * @param bindings The input bindings
     * @return The output bindings from the operation
     */
    public static LinkedList<Binding> testSymbol(String params, LinkedList<Binding> bindings)
    {
        // separate params
        String[] terms = params.split(" ", 3);
        
        Solution sol = getSolution(terms[0]);
        
        
        // remove () around pattern, maybe move to constructor
        terms[2] = terms[2].substring(1, terms[2].length()-1);
        
        
        Pattern pat = new Pattern(terms[2]);
        
        LinkedList<Binding> result = new LinkedList<>();
        
        // for each binding
        for (Binding b : bindings)
        {
            // get value of parameter ?x
            String source = b.getValue(terms[1].substring(1));
            
            // if parameter has not been defined for this binding then make binding fail
            if (source == null)
            {
                Binding failedBinding = new Binding(b);
                failedBinding.setSuccess(false);
                result.add(failedBinding);
                continue;
            }
            
            // add spaces between characters so pattern match on characters not words
            for (int i=1; i<source.length(); i++)
            {
                source = source.substring(0, i) + " " + source.substring(i);
                i++;
            }
            
            // call pattern matcher and store results
            LinkedList<Binding> newBindings = new LinkedList<>();
            newBindings.add(new Binding(b));
            
            result.addAll(patternMatch(source, pat, newBindings ));
        }
        
        return result;
    }
    
    /**
     * Performs an and operation on two sets of bindings. False bindings stay the same. True bindings only remain true if there is an equal binding in the other list which is also true.
     * 
     * @param b1 First bindings list
     * @param b2 Second bindings list
     * @return The output bindings list
     */
    public static LinkedList<Binding> and(LinkedList<Binding> b1, LinkedList<Binding> b2)
    {
        LinkedList<Binding> result = new LinkedList<>();
        
        mainLoop:
        for (int i=0; i<b1.size(); i++)
        {
            Binding ba = b1.get(i);
            
            // if binding is false then put straight into result
            if (!ba.isSuccess())
            {
                result.add(new Binding(ba));
                continue;
            }
            
            // if true then see if find equal in other list
            for (int j=0; j<b2.size(); j++)
            {
                Binding bb = b2.get(j);
                
                if (ba.equals(bb))
                {
                    result.add(new Binding(ba));
                    continue mainLoop;
                }
            }
            
            // couldn't find in other list so create false copy and put in result
            Binding copy = new Binding(ba);
            copy.setSuccess(false);
            
            result.add(copy);
        }
        
        // same as above except for other list
        
        mainLoop:
        for (int i=0; i<b2.size(); i++)
        {
            Binding ba = b2.get(i);

            if (!ba.isSuccess())
            {
                result.add(new Binding(ba));
                continue;
            }

            for (int j=0; j<b1.size(); j++)
            {
                Binding bb = b1.get(j);

                if (ba.equals(bb))
                {
                    result.add(new Binding(ba));
                    continue mainLoop;
                }
            }

            Binding copy = new Binding(ba);
            copy.setSuccess(false);

            result.add(copy);
        }
        
        Binding.clearRepeates(result);
        
        return result;
    }
    
    /**
     * Performs an or operation on two sets of bindings. A false binding will only be in the output list if there is not an equal binding in the other list which is true.
     * 
     * @param b1 First bindings list
     * @param b2 Second bindings list
     * @return The output bindings list
     */
    public static LinkedList<Binding> or(LinkedList<Binding> b1, LinkedList<Binding> b2)
    {
        LinkedList<Binding> result = new LinkedList<>();
        
        mainLoop:
        for (int i=0; i<b1.size(); i++)
        {
            Binding ba = b1.get(i);
            
            // if binding is success then put straight in result
            if (ba.isSuccess())
            {
                result.add(new Binding(ba));
                continue;
            }
            
            // check other list for an equivelant binding except that it is true
            for (int j=0; j<b2.size(); j++)
            {
                Binding bb = b2.get(j);
                
                if (bb.isSuccess() && ba.equalsVar(bb))
                {
                    result.add(new Binding(bb));
                    continue mainLoop;
                }
            }
            
            // could not find one so add this false one to result
            Binding copy = new Binding(ba);
            
            result.add(copy);
        }
        
        // same as above except for other list
        
        mainLoop:
        for (int i=0; i<b2.size(); i++)
        {
            Binding ba = b2.get(i);

            if (ba.isSuccess())
            {
                result.add(new Binding(ba));
                continue;
            }

            for (int j=0; j<b1.size(); j++)
            {
                Binding bb = b1.get(j);

                if (bb.isSuccess() && ba.equalsVar(bb))
                {
                    result.add(new Binding(bb));
                    continue mainLoop;
                }
            }

            Binding copy = new Binding(ba);

            result.add(copy);
        }
        
        Binding.clearRepeates(result);
        
        return result;
    }
    
    /**
     * Performs a not operation on a set of bindings. The output is a copy of the input list except all bindings have their success negated.
     * 
     * @param binding  Input bindings list
     * @return The output bindings list
     */
    public static LinkedList<Binding> not(LinkedList<Binding> binding)
    {
        LinkedList<Binding> result = new LinkedList<>();
        
        // create copy of each binding and negate success and add to result
        for (Binding b : binding)
        {
            Binding copy = new Binding(b);
            copy.setSuccess(!copy.isSuccess());
            
            result.add(copy);
        }
        
        return result;
    }
    
    
    /**
     * Perform a substitute operation which edits the value of a parameter by replacing every occurrence of a term with another term.
     */
//    public static LinkedList<Binding> substitute(String params, LinkedList<Binding> bindings)
//    {
//    }
    
    /**
     * Perform a split operation on a parameter with a given character. The two parts are then placed or tested with two other given parameters (depending if they already have values set).
     * 
     * @param params The parameters for the split operation as a string. Format: "originalBinding newBinding1 newBinding2 character" eg "?ln ?l ?n ."
     * @param bindings The input bindings
     * @return The output bindings from the operation
     */
    public static LinkedList<Binding> split(String params, LinkedList<Binding> bindings)
    {
        // seperate params
        String[] terms = params.split(" ", 4);
        
        LinkedList<Binding> result = new LinkedList<>();
        
        for (Binding b : bindings)
        {
            Binding newBinding = new Binding(b);
            
            String oldValue = b.getValue(terms[0].substring(1));
            
            if (oldValue != null)
            {
                int index = oldValue.indexOf(terms[3]);
            
                if (index > 0)
                {
                    // first half of split
                    
                    String parName = terms[1].substring(1);
                    String newValue = oldValue.substring(0, index);

                    if (newBinding.isDefined(parName))
                    {
                        if (!newBinding.getValue(parName).equals(newValue))
                            newBinding.setSuccess(false);
                    }
                    else
                        newBinding.setValue(parName, newValue);

                    
                    // second part of split

                    parName = terms[2].substring(1);

                    if (index >= oldValue.length())
                        newValue = ""; // if nothing after split character then value is blank
                    else
                        newValue = oldValue.substring(index+1);


                    if (newBinding.isDefined(parName))
                    {
                        if (!newBinding.getValue(parName).equals(newValue))
                            newBinding.setSuccess(false);
                    }
                    else
                        newBinding.setValue(parName, newValue);
                }
            }
            // if cannot split just pass binding through without change
            
            
            
            result.add(newBinding); 
        }
        
        return result;
    }
    
}
