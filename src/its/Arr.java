package its;

import java.util.LinkedList;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 *
 * @author Ryan Marker
 */
public class Arr implements MemoryObject
{
    private boolean logged = false;
    
    private final VBox root = new VBox();
    private final HBox arrBox = new HBox();
    private final VBox varBox = new VBox();
    
    private final Text location = new Text();
    
    private final TextField name = new TextField();
    private final TextField type = new TextField();
    private final Spinner<Integer> size = new Spinner<>(0, 50, 0, 1);
    
    
    private final CheckBox isPointer = new CheckBox();
    
    
    private int frame;
    
    
   public LinkedList<Var> variables = new LinkedList<>();
    
    private final String NOTPTR = "Not pointer";
    private final String PTR = "* (Pointer)";
    
    public Arr(boolean log)
    {
        root.setPadding(new Insets(10));
        root.setSpacing(5);
        
        arrBox.setPadding(new Insets(10));
        arrBox.setSpacing(5);
        
        arrBox.setOnMouseClicked((MouseEvent me) -> {   
            ToggleButton selected = (ToggleButton) (ITS.symbolTable.group.getSelectedToggle());
            
            if (selected.getText().equalsIgnoreCase("delete"))
            {
                // one will fail, unless same object added to both, fail should have no bad consequences
                ITS.stack.removeMemObj(this);
                ITS.heap.removeMemObj(this);
            }
            
        });
        
        varBox.setPadding(new Insets(20));
        varBox.setAlignment(Pos.CENTER_RIGHT);
        
        name.setText("name");
        name.setTooltip(new Tooltip("name"));
        type.setText("type");
        type.setTooltip(new Tooltip("type"));
        
        isPointer.setTooltip(new Tooltip("is pointer"));
        
        size.setEditable(true);
        size.setTooltip(new Tooltip("size"));
        

        
        arrBox.getChildren().addAll(location, name, type, isPointer, size);
        
        root.getChildren().addAll(arrBox, varBox);
        
        
        // maybe change to key listeners in order to avoid the possibility of making changes which turns this into an infinit loop.
        

        name.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            setName(newValue);
        });
        

        type.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            setType(newValue);
        });
        

        isPointer.setOnAction((ActionEvent ae) -> {
            setPointer(isPointer.isSelected());
        });
        
        size.valueProperty().addListener((ObservableValue<? extends Integer> observable, Integer oldValue, Integer newValue) -> {
            // test if infinite loop with setSize

            setSize(newValue);
        });
        
        
        logged = log;
    }

    @Override
    public boolean isLogged()
    {
        return logged;
    }

    
    @Override
    public void setName(String name)
    {
        this.name.setText(name);
        
        int i = 0;
        
        for (Var v : variables)
        {
            v.setName(name + "[" + i + "]");
            i++;
        }
    }

    @Override
    public String getName()
    {
        return name.getText();
    }

    @Override
    public void setType(String type)
    {
        this.type.setText(type);
        
        for (Var v : variables)
        {
            v.setType(type);
        }
    }

    @Override
    public String getType()
    {
        return type.getText();
    }
    
    public void setPointer(boolean value)
    {
        isPointer.setSelected(value);
        
        for (Var v : variables)
        {
            v.setPointer(value);
        }
    }
    
    public Boolean isPointer()
    {
        return isPointer.isSelected();
    }
    
    public void setSize(int newSize)
    {
        //this.size.setText(Integer.toString(newSize));
        this.size.getValueFactory().setValue(newSize);
        
        int currentSize = getSize();
        
        while (newSize > currentSize)
        {
            Var v = new Var(false, isLogged());
            v.setName(name.getText() + "[" + currentSize + "]");
            v.setLocked(1, true);
            v.setType(type.getText());
            v.setLocked(2, true);
            v.setPointer(isPointer.isSelected());
            v.setLocked(4, true);
            v.setFrame(frame);

            variables.add(v);
            varBox.getChildren().add(v.getRoot());

            currentSize ++;
        }

        while (newSize < currentSize)
        {
            currentSize --;

            varBox.getChildren().remove(varBox.getChildren().size()-1);
            variables.remove(variables.size() - 1);
        }
    }
    
    public int getSize()
    {
        return variables.size();
    }
    
    
    public void setValue(int index, String value)
    {
        variables.get(index).setValue(value);
    }

    @Override
    public void setFrame(int frame)
    {
        this.frame = frame;
        
        if (frame < 0)
        {
            location.setText("Heap.");
        }
        else
        {
           location.setText("Frame" + frame + "."); 
        }
        
        for (Var v : variables)
        {
            v.setFrame(frame);
        }
    }

    @Override
    public int getFrame()
    {
        return frame;
    }

    @Override
    public String getLocation()
    {
        return location.getText();
    }
    
    
    
    @Override
    public void setLocked(boolean l)
    {
        getRoot().setMouseTransparent(l);
        
        name.setEditable(!l);
        type.setEditable(!l);
        size.setEditable(!l);
        isPointer.setDisable(l);
        
        for (Var v : variables)
        {
            v.setLocked(l);
        }
    }

    @Override
    public void setHighlight(Color colour)
    {
        if (colour == null)
        {
            getRoot().setEffect(null);
        }
        else
        {
            getRoot().setEffect(new DropShadow(20, colour));
        }
        
    }
    

    @Override
    public String generateTextRep()
    {
        String result = "";
        
        for (Var v : variables)
        {
            result += v.generateTextRep();
        }
        
        return result;
    }

    @Override
    public double getArrowX()
    {
        return 0;
    }

    @Override
    public double getArrowY()
    {
        return 0;
    }
    
    
    @Override
    public Node getRoot()
    {
        return root;
    }

}
