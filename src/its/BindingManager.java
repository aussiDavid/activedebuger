package its;

import java.util.ArrayList;

public class BindingManager {
	ArrayList<Binding> bindingsCollection = new ArrayList<>();
	
	
	public void clearBindings()
	{
		this.bindingsCollection = new ArrayList<>();
	}
	
	public void addBinding(Binding bindingToAdd)
	{
		this.bindingsCollection.add(bindingToAdd);
	}
	
	public int getFeedbackCount(Binding bindingToCheck)
	{
		for(Binding bindingElement : this.bindingsCollection)
		{
			if(bindingElement.equals(bindingToCheck))
			{
				
				//bindingElement.timesSeenAlready++;
				return bindingElement.timesSeenAlready - 1;
			}
		}
		return 0;
	}
	public Binding alreadyStored(Binding bindingToCheck)
	{
		for(Binding bindingElement : this.bindingsCollection)
		{
			if(bindingElement.equals(bindingToCheck))
			{
				return bindingElement;
				//bindingElement.timesSeenAlready++;
				//return bindingElement.timesSeenAlready - 1;
			}
		}
		return null;
	}
}
