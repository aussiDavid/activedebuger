package its;

import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 *
 * @author Ryan Marker
 */
public class ProblemInterface implements NodeGraphics
{

    private int probNum;
    private Problem prob;
    
    private final Text probNumDisplay = new Text();
    
    private final VBox root = new VBox();
    
    private final ScrollPane scrollPane = new ScrollPane();
    
    private final HBox topBtns = new HBox();
    private final HBox bottomBtns = new HBox();
    
    private final Button prevBtn = new Button("Previous Problem");
    private final Button nextBtn = new Button("Next Problem");
    private final Button startBtn = new Button("Start");
    private final Button stepBtn = new Button("Step");
    private final Button continueBtn = new Button("Continue");
    
    private final TextField breakLineField = new TextField();
    
    private final ArrayList<Line> lines = new ArrayList<>();
    
    private final VBox vLines = new VBox();
    
    private final Separator sep = new Separator(Orientation.HORIZONTAL);
    
    public final HBox idBox = new HBox();
    public final Label userLabel = new Label("ID:");
    public final Label userID = new Label();
    public final Button switchUser = new Button("Switch User");
    private final Button help = new Button("?");
    
    public ProblemInterface()
    {
        vLines.setSpacing(4);
        vLines.setPadding(new Insets(10));
        
        scrollPane.setContent(vLines);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        VBox.setVgrow(scrollPane, Priority.ALWAYS);
        
        topBtns.setSpacing(10);
        topBtns.getChildren().addAll(prevBtn, nextBtn);
        
        breakLineField.setPrefColumnCount(4);
        
        bottomBtns.setSpacing(10);
        bottomBtns.getChildren().addAll(startBtn, stepBtn, continueBtn, breakLineField);
        
        idBox.setSpacing(10);
        idBox.getChildren().addAll(userLabel, userID, switchUser, help);
        
        root.setPadding(new Insets(10));
        root.setSpacing(10);
        root.getChildren().addAll(idBox, probNumDisplay, topBtns, scrollPane, bottomBtns, sep);
        
        
        switchUser.setTooltip(new Tooltip("Change the current user ID"));
        switchUser.setOnAction((ActionEvent ae) -> {
            ITS.getAndSetID();
        });
        
        help.setTooltip(new Tooltip("Watch introduction video again"));
        help.setOnAction((ActionEvent ae) -> {
            ITS.displayHelp();
        });
        
        
        prevBtn.setTooltip(new Tooltip("Move to the previous problem in the sequence"));
        prevBtn.setOnAction((ActionEvent ae) -> {
            if (probNum > 0)
            {
                setProblemNum(probNum - 1);
                //start(); uneeded
                ITS.updateIS();
            }
            
        });
        
        nextBtn.setTooltip(new Tooltip("Move to the next problem in the sequence"));
        nextBtn.setOnAction((ActionEvent ae) -> {
            if (probNum +1 < ITS.problems.size())
            {
                setProblemNum(probNum + 1);
                //start(); uneeded
                ITS.updateIS();
            }
            
        });
        
        startBtn.setTooltip(new Tooltip("Reset the current problem to the beginning"));
        startBtn.setOnAction((ActionEvent ae) -> {
            start();
            ITS.updateIS();
        });
        
        stepBtn.setTooltip(new Tooltip("Exeute the highlighted line"));
        stepBtn.setOnAction((ActionEvent ae) -> {
            ITS.printLogMsg("Line: " + CInterpreter.getCurLine() + ", step");
            
            step();
            
            
        });
        
        continueBtn.setTooltip(new Tooltip("The program will be executed line by line, untill the end is reached or it reaches the line number corresponding to that in the nearby box. If the box is empty then if will continue to the end of the program."));
        continueBtn.setOnAction((ActionEvent ae) -> {
            ITS.printLogMsg("Line: " + CInterpreter.getCurLine() + ", continue to line: " + breakLineField.getText());
            
            try
            {
                String breakLineText = breakLineField.getText();
                int breakLine;
                
                if (breakLineText.isEmpty())
                {
                    breakLine = Integer.MAX_VALUE;
                }
                else
                {
                    breakLine = Integer.parseInt(breakLineField.getText());
                }

                // make sure to do at least one next if can
                boolean first = true;
                boolean cont = true;

                while ( (CInterpreter.getCurLine() != breakLine  && cont == true ) || first == true)
                {
                    first = false;

                    cont = step();
                }
            }
            catch (NumberFormatException ex)
            {
                
            }
            
        });
          
    }
    
    public void start()
    {
        ITS.printLogMsg("Start poblem: " + probNum);
        
        CInterpreter.start(prob);
        
        clearHighlight();
        
        setIsCurLine(0, true);
    }
    
    private boolean step()
    {
        // if reached end then show message
        if (CInterpreter.isFinished())
        {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("End of program");
            alert.setHeaderText(null);
            alert.setContentText("Reached end of program");

            alert.showAndWait();
            
            return false;
        }
        
        // check if cannot pass roadblock
        if (prob.isRoadBlock(CInterpreter.getCurLine()) && !prob.hasPassedLine(CInterpreter.getCurLine()))
        {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Must solve line");
            alert.setHeaderText(null);
            alert.setContentText("You need to ask for feedback at this line, and correctly represent memory to continue");

            alert.showAndWait();
            
            return false;
        }


        CInterpreter.next();
        
        
        // update progress graph of current problem
        if (ITS.progress.getNextSeqNum() == CInterpreter.getStepNum())
        {
            ITS.progress.addLine(CInterpreter.getCurLine());
        }
        

        clearHighlight();

        
        // maybe make a generic line selected function so that this code is not linked with step
        
        if (CInterpreter.getCurLine() - 1 < lines.size())
        {
            if(prob.isRoadBlock(CInterpreter.getCurLine()) && !prob.hasPassedLine(CInterpreter.getCurLine()))
                  lines.get(CInterpreter.getCurLine()-1).setStrikethrough(true);

            setIsCurLine(CInterpreter.getCurLine() - 1, true);
        }


        ITS.updateIS();
        
        return true;
    }
    
    private void setIsCurLine(int lineNum, boolean isHighlighted)
    {
        // maybe change to setCurLine and have it unset all lines and then highlight only the particular line. Then would not need second parameter
        
        Line line = lines.get(lineNum);
        
        line.setSeperator(isHighlighted);
        line.setTextColour(isHighlighted);
        
        ITS.progress.setSelectedStep(CInterpreter.getStepNum());
    }
    
    
    private void clearHighlight()
    {
        lines.stream().forEach((Line l) -> {l.clearHighlight();});
    }

    public void setProblemNum(int num)
    {
        if (num < 0 || num >= ITS.problems.size())
            return;
        
        probNum = num;
        
        probNumDisplay.setText("Problem: " + num);
        
        setProblem(ITS.problems.get(num));
    }
    
    public int getProblemNum()
    {
        return probNum;
    }
    
    private void setProblem(Problem p)
    {
        // clear any feedback messages
        ITS.feedback.clearFeedback();
        
        ITS.bindingManager.clearBindings();
        
        
        prob = p;
        
      //Set feedback records TODO
        int totalLines = p.getNumLines();
        System.out.println("Create feedback array of size: " + totalLines);
        ITS.lineFeedback = new ArrayList<ArrayList<String > >(totalLines);
        for(int i = 0 ; i < totalLines ; i++)
        {
        	ITS.lineFeedback.add(new ArrayList<String>());
        }
        
        vLines.getChildren().clear();
        
        int tabs = 0;
        
        int lineNumSize = Integer.toString(p.getNumLines()).length();
        
        for (int i=0; i<p.getNumLines(); i++)
        {
            if (lines.size() <= i)
            {
                Line newLine = new Line();
                
                lines.add(newLine);
            }
            
            String lineString = p.getLine(i+1);
            
            if (lineString.equals("}"))
                tabs --;
            
            Line line = lines.get(i);
            
            String numString = Integer.toString(i+1);
            
            while (numString.length() < lineNumSize)
                numString = "0" + numString;
            
            numString += ": \t";
            
            for (int j=0; j<tabs; j++)
            {
                numString += "\t";
            }
            
            
            line.setText(lineString);
            line.setLineNum(numString);

            line.setTextColour(false);
            
            line.setNumColour(prob.hasPassedLine(i+1));
            
            vLines.getChildren().add(line.getRoot());
            
            if (lineString.equals("{"))
                tabs ++;
            
        }
        
        start();
        
        // update progress interface to show new problem
        ITS.progress.setProblemGraph(probNum);
        
        // make sure progress interface shows first line
        if (ITS.progress.getNextSeqNum() == CInterpreter.getStepNum())
        {
            ITS.progress.addLine(1);
        }
    }
    
    public void updateDisplay()
    {
        for (int i=0; i<prob.getNumLines(); i++)
        {
            Line line = lines.get(i);
            
            line.setNumColour(prob.hasPassedLine(i+1));
            
            if (!prob.isRoadBlock(i+1) && prob.hasPassedLine(i+1))
                line.setStrikethrough(false);
        }  
    }
    
    
    @Override
    public Node getRoot()
    {
        return root;
    }
    
    
    private static class Line implements NodeGraphics
    {
        private final double FONT_SIZE = 14;
        
        private final VBox root = new VBox();
        
        private final HBox hor = new HBox();
        
        private final Text text = new Text();
        private final Text num = new Text();
        
        private final Separator sep = new Separator(Orientation.HORIZONTAL);

        public Line()
        {
            text.setFont(Font.font(FONT_SIZE));
            num.setFont(Font.font(FONT_SIZE));
            
            hor.getChildren().addAll(num, text);
            
            root.getChildren().add(hor);
        }
        
        public void setText(String text)
        {
            this.text.setText(text);
        }
        
        public void setLineNum(String numText)
        {
            num.setText(numText);
        }
        
        public void setTextColour(boolean isHighlighted)
        {
            if (isHighlighted)
            {
                text.setFill(Paint.valueOf("blue"));
            }
            else
            {
                text.setFill(Paint.valueOf("black"));
            }
                
            
        }
        
        public void setNumColour(boolean isPassed)
        {
            // disabled for now
            /*
            if (isPassed)
                num.setFill(Paint.valueOf("green"));
            else
                num.setFill(Paint.valueOf("black"));
            */
        }
        
        public void setStrikethrough(boolean isStrikethrough)
        {
            text.setStrikethrough(isStrikethrough);
        }
        
        public void setSeperator(boolean hasSeperator)
        { 
            if (hasSeperator && !root.getChildren().contains(sep))
                root.getChildren().add(0, sep);
            else if (!hasSeperator && root.getChildren().contains(sep))
                root.getChildren().remove(0);
        }
        
        public void clearHighlight()
        {
            setTextColour(false);
            setStrikethrough(false);
            setSeperator(false);
        }

        @Override
        public Node getRoot()
        {
            return root;
        }
    }
    
}
