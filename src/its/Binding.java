package its;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Set;

/**
 *
 * @author Ryan Marker
 */
public class Binding
{

	//Used by BindingManager and feedback manager
	public int timesSeenAlready = 0;
	
	
	//public getFeedback
	
    /**
     * Removes any duplicate bindings in the list. A duplicate is determined by {@link equals()}
     * 
     * @param b The list to remove duplicates from
     */
    public static void clearRepeates(LinkedList<Binding> b)
    {
        for (int i=0; i<b.size(); i++)
        {
            Binding b1 = b.get(i);
            
            for (int j=i+1; j<b.size(); j++)
            {
                Binding b2 = b.get(j);
                
                if (b1.equals(b2))
                {
                    b.remove(j);
                    j--;
                }
            }
        }
    }
    
    private boolean success;
    private HashMap<String, String> var;
    
    public Binding()
    {
        success = true;
        var = new HashMap<>();
    }
    
    public Binding(Binding copy)
    {
        success = copy.isSuccess();
        
        var = copy.getVarCopy();
    }

    public void setSuccess(boolean success)
    {
        this.success = success;
    }

    public boolean isSuccess()
    {
        return success;
    }
    
    public boolean isDefined(String var)
    {
        return this.var.containsKey(var);
    }
    
    public String getValue(String key)
    {
        return var.get(key);
    }
    
    public boolean setValue(String key, String value)
    {
       return var.putIfAbsent(key, value) == null;
    }
    
    public HashMap<String, String> getVarCopy()
    {
        return new HashMap<>(var);
    }

    /**
     * Checks if the values stored for the two bindings are equal.
     * 
     * @param bind The binding to check if its values are equal.
     * @return True if the same keys are stored with the same values, else false.
     */
    public boolean equalsVar(Binding bind)
    {
        Set<String> keySet = var.keySet();
        Set<String> keySet2 = bind.var.keySet();
        
        if (!keySet.equals(keySet2))
            return false;
        
        for (String s : keySet2)
        {
            if (!var.get(s).equals(bind.var.get(s)))
                return false;
        }
        
        return true;
    }
    
    /**
     * Checks if the binding is equal to this binding, including the values stored and the success state.
     * 
     * @param bind Te binding to check.
     * @return True or False.
     */
    public boolean equals(Binding bind)
    {
        if (isSuccess() != bind.isSuccess())
            return false;
        
        return equalsVar(bind);
    }

    @Override
    public String toString()
    {
        String result = "(";
        
        result += isSuccess() + ", " + var.toString() + " )";
        
        return result;
    }
    
    
}
