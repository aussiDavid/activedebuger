package its;

import java.util.LinkedList;

/**
 *
 * @author Ryan Marker
 */
public class CInterpreter
{
    
    private static Problem prob;
    private static Solution sol;
    
    private static int curLine;
    
    private static int stepNum;
    
    private static int curFrame;
    private static int curHeap;
    
    
    private static final LinkedList<Scope> scopes = new LinkedList<>();
    
    /**
     * Initialise the interpreter to the beginning of the given problem.
     * 
     * @param p The problem the interpreter should use
     */
    public static void start(Problem p)
    {
        prob = p;
        sol = new Solution();
        curLine = 1;
        curFrame = 0;
        curHeap = 0;
        stepNum = 0;
        
        scopes.clear();
        scopes.add(new Scope(0));
    }
    
    /**
     * Restart the current problem from the beginning
     */
    public static void start()
    {
        start(prob);
    }
    
    /**
     * Move the interpreter one active line through the problem
     */
    public static void next()
    {
        
        // execute line
        curLine = computeLine(prob.getLine(curLine));
        
        // skip empty lines
        while (curLine <= prob.getNumLines() && prob.getLine(curLine).isEmpty())
        {
            curLine ++;
        }
        
        stepNum ++;
    }
    
    /**
     * Whether the interpreter has reached the end of the current problem
     * @return True if it has reached the last line of the problem
     */
    public static boolean isFinished()
    {
        if (curLine > prob.getNumLines())
            return true;
        
        return false;
    }
    
    /**
     * Returns what line the interpreter is up to
     * 
     * @return The next line to be executed by the interpreter
     */
    public static int getCurLine()
    {
        return curLine;
    }
    
    /**
     * Returns the step number that the interpreter is up to
     * 
     * @return The internal step number
     */
    public static int getStepNum()
    {
        return stepNum;
    }
    
    /**
     * Returns the current state of the ideal solution created by the interpreter
     * @return The Solution
     */
    public static Solution getSolution()
    {
        return sol;
    }
    
    /**
     * Get the final solution for the given problem
     * 
     * @param p The problem
     * @return The solution
     */
    public static Solution getSolution(Problem p)
    {
        return getSolution(p, p.getNumLines());
    }
    
    /**
     * Starts the interpreter with the given problem and incrementally builds the solution up to the specified line inclusively, and returns the ideal solution.
     * 
     * @param p The problem to use.
     * @param line The line number (starting from 1) step through until (inclusively).
     * @return The ideal solution of the problem after stepping through to the given line.
     */
    public static Solution getSolution(Problem p, int line)
    {
        start(p);
        
        while (getCurLine() <= line)
        {
            next();
        }
        
        return getSolution();

    }
    
    private static void createScope(int callLine)
    {
        Scope newScope = new Scope(callLine);
        
        scopes.add(newScope);
    }
    
    private static void removeScope()
    {
        LinkedList<String> names = scopes.getLast().varNames;
        
        // remove all var in most recent scope from the solution
        for (String n : names)
        {
            removeVar(n);
        }
        
        scopes.removeLast();
    }
    
    private static void createStackVar(String var)
    {
        String name = var.split(" ")[0];
        
        // add var to most recent scope
        scopes.getLast().varNames.addLast(name);
        
        sol.addVar(var);
    }
    
    private static boolean removeVar(String name)
    {
        return sol.removeVar(name);
    }
    
    private static int computeLine(String line)
    {
        // trim
        line = line.trim();
        
        
        
        // check if "if" or "while"
        if (line.contains("if") || line.contains("while"))
        {
            int start = line.indexOf('(');
            int end = Constraint.findCloseBracket(line.substring(start));
            
            String cond = line.substring(start+1, start+end);
            
            if (condition(cond))
            {
                createScope(getCurLine());
                return getCurLine() + 2;
            }
            else
            {
                int balance = 1;
                
                int i = 2;
                
                while (balance > 0)
                {
                    String check = prob.getLine(getCurLine() + i);
                    
                    if (check.equals("{"))
                        balance ++;
                    else if (check.equals("}"))
                        balance --;
                    
                    i++;
                }
                
                return getCurLine() + i;
            }
        }
        
        // check if for
        if (line.contains("for"))
        {
            int start = line.indexOf('(');
            int end = Constraint.findCloseBracket(line.substring(start));
            
            createScope(getCurLine());
            
            String[] parts = line.substring(start+1, start+end).split(";");
            
            allocation(parts[0].split(" ")[0], parts[0].split(" ")[1]);
            assignment(parts[0].split(" ")[1], parts[0].split(" ", 4)[3]);
            
            String cond = parts[1].trim();
            
            
            // make sure cond is in form var op expression
            String[] condSplit = cond.split(" ");
            
            if (condSplit.length > 3)
            {
                cond = condSplit[0] + " " + condSplit[1] + " ";
                
                for (int i=2; i<condSplit.length; i++)
                    cond = cond + condSplit[i];
            }
            
            if (condition(cond))
            {
                createScope(getCurLine());
                return getCurLine() + 2;
            }
            else
            {
                int balance = 1;
                
                int i = 2;
                
                while (balance > 0)
                {
                    String check = prob.getLine(getCurLine() + i);
                    
                    if (check.equals("{"))
                        balance ++;
                    else if (check.equals("}"))
                        balance --;
                    
                    i++;
                }
                
                return getCurLine() + i;
            }
        }
        
        
        // remove ;
        if (line.lastIndexOf(';') > 0)
            line = line.substring(0, line.lastIndexOf(';'));
        
        
        // check if at end of block with }
        
        if (line.startsWith("}"))
        {
            removeScope();
            
            // can probably change to use call line of last scope
            int startLineNum = prob.findBracket(curLine, -1) - 1;
            String startLine = prob.getLine(startLineNum);
            
            if (startLine.contains("while"))
            {
                if (condition(startLine.substring(startLine.indexOf('(') + 1, startLine.indexOf(')'))))
                {
                    createScope(startLineNum);
                    return startLineNum + 2;
                }
                    
            }
            else if (startLine.contains("for"))
            {
                int first = startLine.indexOf(';');
                int second = startLine.indexOf(';', first+1);
                
                String command = startLine.substring(second + 1, startLine.indexOf(')')).trim();

                // currently just taken from below will need to keep current

                // check if assignment
                int equIndex = command.indexOf('=');
                if (equIndex > 0)
                {
                    assignment(command.substring(0, equIndex).trim(), command.substring(equIndex+1).trim());
                }


                // check if ++
                if (command.contains("++"))
                {
                    String token = command.substring(0, command.length()-2);

                    String varName = getDestVarName(token);

                    sol.editVar(varName, 4, Double.toString(Double.parseDouble(sol.getVar(varName).split(" ")[4]) + 1)); // maybe need to check if int or double

                }

                // check if --
                if (line.contains("--"))
                {
                    String token = command.substring(0, command.length()-2);

                    String varName = getDestVarName(token);

                    sol.editVar(varName, 4, Double.toString(Double.parseDouble(sol.getVar(varName).split(" ")[4]) - 1)); // maybe need to check if int or double

                }
                
                if (condition(startLine.substring(first + 1, second)))
                {
                    
                    createScope(startLineNum);
                    return startLineNum + 2;
                }
                else
                {
                    // need to remove extra scope for the allocation in for
                    removeScope();
                }
            }
            
            String checkLine;
            int i = 1;
            int balance = 0;
            
            checkLine = prob.getLine(curLine + i);
            
            while (checkLine.startsWith("else") || checkLine.startsWith("{") || balance != 0)
            {
                if (checkLine.startsWith("{"))
                    balance ++;
                else if (checkLine.startsWith("}"))
                    balance --;
                
                i++;
                
                checkLine = prob.getLine(curLine + i);
            }
            
            
            return curLine + i;
            
        }
        
        
        // check if "else" and if so then just do it as skipping else is part of "}"
        if (line.equals("else"))
        {
            createScope(getCurLine());
            return curLine + 2;
        }
        
        
        // check if assignment
        int equIndex = line.indexOf('=');
        if (equIndex > 0)
        {
            return assignment(line.substring(0, equIndex).trim(), line.substring(equIndex+1).trim());
        }
        
        
        // check if ++
        if (line.contains("++"))
        {
            String token = line.substring(0, line.length()-2);
            
            String varName = getDestVarName(token);
            
            sol.editVar(varName, 4, Double.toString(Double.parseDouble(sol.getVar(varName).split(" ")[4]) + 1)); // maybe need to check if int or double
            
            return getCurLine() + 1;
        }
        
        // check if --
        if (line.contains("--"))
        {
            String token = line.substring(0, line.length()-2);
            
            String varName = getDestVarName(token);
            
            sol.editVar(varName, 4, Double.toString(Double.parseDouble(sol.getVar(varName).split(" ")[4]) - 1)); // maybe need to check if int or double
            
            return getCurLine() + 1;
        }
        
        
        // check if function call
        if (line.contains("("))
        {
            return callFunc(line);
        }
        
        // check if return
        if (line.contains("return"))
        {
            String returnValue = getValue(line.substring(6).trim());
            
            int callLineNum = scopes.getLast().callLine;
            
            // don't remove final scope instead end program
            if (callLineNum <=0 || curFrame <= 0)
            {
                return prob.getNumLines() + 1;
            }
            
            String callLine = prob.getLine(callLineNum);
            
            // remove any scopes created in this frame
            // maybe change so that scopes store the frame they belong to so can just remove frames while penultimate scope is in current frame
            while (callLine.contains("if") || callLine.contains("while"))
            {
                removeScope();
                
                callLineNum = scopes.getLast().callLine;
                callLine = prob.getLine(callLineNum);
            }
            
            
            
            curFrame --;
            removeScope();
            
            
            
            int index = callLine.indexOf('=');
            
            if (index > 0)
            {
                assignment(callLine.substring(0, index).trim(), returnValue);
            }
            
            return callLineNum + 1;
        }
        
        
        // check if delete[]
        if (line.contains("delete[]"))
        {
            // get pointer
            String ptrName = line.split(" ")[1];
            
            // get value
            String ptrValue = sol.getVar(getDestVarName(ptrName)).split(" ")[4];
            
            
            int i = 0;
            while (true)
            {
                if (removeVar(ptrValue + "[" + i + "]") == false)
                    break;
                
                i++;
            }
            
            return getCurLine() + 1;
        }
        
        // check if delete
        if (line.contains("delete"))
        {
            // get pointer
            String ptrName = line.split(" ")[1];
            
            // get value
            String ptrValue = sol.getVar(getDestVarName(ptrName)).split(" ")[4];
            
            removeVar(ptrValue);
            
            return getCurLine() + 1;
        }
        
   
        // must be allocation
        String[] tokens = line.split(" ");
        allocation(tokens[0], tokens[1]);
        return getCurLine() + 1;
        
    }
    
    private static int assignment(String des, String source)
    {
        // check for special cases where assignment cannot be done immediently as need to move to a different line

        // check if is function call
        if (source.contains("("))
        {
            return callFunc(source);
        }
        
        
        // standard assignment which can be done as a single line
        
        String varName = getDestVarName(des);
        
        sol.editVar(varName, 4, getValue(source));
                 
        return getCurLine() + 1;
        
    }
    
    private static String getDestVarName(String token)
    {
        String varName;
        
        if (token.startsWith("*"))
        {
            varName = ""; // ensure is initilised
            
            int index;
            
            String var;
            
            while ((index = token.lastIndexOf('*')) >= 0)
            {
                varName = "Frame" + curFrame + "." + token.substring(index+1);
            
                var = sol.getVar(varName);
                
                // only need to account for Frame. at first when reading var from code, after that proper names are used as taken from value in sol
                if (var == null)
                {
                    varName = token.substring(index+1);
            
                    var = sol.getVar(varName);
                }

                if (var == null)
                {
                    varName = varName + "[0]";
                    
                    var = sol.getVar(varName);
                }
                
                
                if (var == null)
                    return null;

                
                varName = var.split(" ")[4];
                
                token = token.substring(0, index) + varName;
            }
            
            var = sol.getVar(varName);

            if (var == null)
            {
                varName = varName + "[0]";

                var = sol.getVar(varName);
            }
            
            if (var == null)
            {
                varName = token;

                var = sol.getVar(varName);
            }
            
            if (var == null)
            {
                return null;
            }
            
            return varName;
        }
        
        int index = token.indexOf('[');
        
        if (index > 0)
        {
            if (token.endsWith("]"))
            {
                varName = "Frame" + curFrame + "." + token.substring(0);

                String var = sol.getVar(varName);

                if (var == null)
                {
                    // try evaluating index

                    String element = getValue(token.substring(index+1, token.length()-1));
                    varName = "Frame" + curFrame + "." + token.substring(0, index+1) + element + "]";
                    var = sol.getVar(varName);
                    
                    if (var != null)
                    {
                        return varName;
                    }

              
                    varName = "Frame" + curFrame + "." + token.substring(0, token.indexOf('['));

                    var = sol.getVar(varName);
                    
                    if (var != null)
                    {
                        String varValue = var.split(" ")[4];
                        
                        varName = varValue + "[" + element + "]";
                        
                        String var2 = sol.getVar(varName);
                        
                        if (var2 != null)
                            return varName;
                        
                        
                        varName = varValue.substring(0, varValue.indexOf('[')) + "[" + element + "]";
                        
                        return varName;
                    }

                    
                }
            }
            
            if (sol.getVar("Frame" + curFrame + "." + token.substring(0, index)) != null)
            {
                int i = Integer.parseInt(token.substring(index+1, token.length()-1));

                String var = sol.getVar("Frame" + curFrame + "." + token.substring(0, index));

                String value = var.split(" ")[4];

                if (value.endsWith("]"))
                {
                    index = value.indexOf('[');
                    int i2 = Integer.parseInt(value.substring(index+1, value.length()-1));

                    varName = value.substring(0, index) + "[" + (i+i2) + "]";

                    return varName;
                }

                varName = value.substring(0) + "[" + i + "]";

                return varName;
            }

            
        }
        
        
        if (sol.getVar("Frame" + curFrame + "." + token) != null)
        {
            varName = "Frame" + curFrame + "." + token.substring(0);
        }
        else
        {
            return null;
        }
        
        return varName;
    }
    
    
    private static void allocation(String token1, String token2)
    {
        int size = 0;
                
        String type = "";

        boolean isPointer = false;


        int index = token2.indexOf('[');

        if (index >= 0)
        {
            int index2 = token2.indexOf(']');

            size = Integer.parseInt(token2.substring(index+1, index2));
            token2= token2.substring(0, index);
        }

        index = token1.lastIndexOf('*');

        if (index >= 0)
        {
            isPointer = true;
            type = token1.substring(0, index);
        }
        else
        {
            type = token1.substring(0);
        }


        if (size == 0)
        {
            createStackVar("Frame" + curFrame + "." + token2.replaceAll(";", "") + " true " + type + " " + isPointer + " uninitialised");
        }
        else
        {
            for (int i=0; i<size ; i++)
            {
                createStackVar("Frame" + curFrame + "." + token2.replaceAll(";", "") + "[" + i + "]" + " true " + type + " " + isPointer + " uninitialised");
            }
        }
    }
    
    private static boolean condition(String cond)
    {
        cond = cond.trim();
        
        String[] tokens = cond.split(" ");
        
        if (tokens.length == 3)
        {
            tokens[0] = tokens[0].replace("(", "");
            tokens[2] = tokens[2].replace(")", "");
            
            String lh = getValue(tokens[0].trim());
            String rh = getValue(tokens[2].trim());

            switch (tokens[1])
            {
                case  "==":
                    return lh.equals(rh);


                case ">":
                    return Double.parseDouble(lh) > Double.parseDouble(rh);


                case ">=":
                    return Double.parseDouble(lh) >= Double.parseDouble(rh);


                case "<":
                    return Double.parseDouble(lh) < Double.parseDouble(rh);


                case "<=":
                    return Double.parseDouble(lh) <= Double.parseDouble(rh);
                    
                case "!=":
                    return Double.parseDouble(lh) != Double.parseDouble(rh);

            }
            
            return false;
        }
        
        
        
        int index1;
        int index2;
        
        if (cond.startsWith("("))
        {
            boolean first;
            
            int index = Constraint.findCloseBracket(cond);
            
            first = condition(cond.substring(1, index));
            
            index1 = cond.indexOf("&&", index);
            index2 = cond.indexOf("||", index);
            
            if (index1 <= 0)
                index1 = Integer.MAX_VALUE;
        
            if (index2 <= 0)
                index2 = Integer.MAX_VALUE;
        
            int min = Math.min(index1, index2);
            
            if (min < cond.length())
            {
                if (cond.subSequence(min, min+2).equals("&&"))
                {
                    return first && condition(cond.substring(min+3));
                }
                else
                {
                    return first || condition(cond.substring(min+3));
                }
             
            }
            else
                return first;
        }
        
        
        index1 = cond.indexOf("&&");
        index2 = cond.indexOf("||");
        
        if (index1 <= 0)
            index1 = Integer.MAX_VALUE;
        
        if (index2 <= 0)
            index2 = Integer.MAX_VALUE;
        
        int index = Math.min(index1, index2);
        
        if (tokens[3].equals("&&"))
        {
            return condition(cond.substring(0, index)) && condition(cond.substring(index + 3));
        }
        else
        {
            return condition(cond.substring(0, index)) || condition(cond.substring(index + 3));
        }
        
    }
    
    private static String getValue(String source)
    {
        if (source.startsWith("&"))
        {
            String varName2 = getDestVarName(source.substring(1));
            return varName2;
        }
        
        
        String[] tokens = source.split(" ");
        
        String varName2 = getDestVarName(source);
        
        if (varName2 != null)
        {
            String var2 = sol.getVar(varName2);
            
            return var2.split(" ")[4];
        }
         
        
        
        if (sol.getVar("Frame" + curFrame + "." + tokens[0] + "[0]") != null)
        {
            return "Frame" + curFrame + "." + tokens[0] + "[0]";
        }
        
        if (tokens[0].equals("new"))
        {
            // make new var on heap

            int size = 0;

            String type = "";

            boolean isPointer = false;


            int index = tokens[1].indexOf('(');

            if (index >= 0)
            {

            }

            index = tokens[1].indexOf('[');

            if (index >= 0)
            {
                int index2 = tokens[1].indexOf(']');

                size = Integer.parseInt(getValue(tokens[1].substring(index+1, index2)));
                tokens[1] = tokens[1].substring(0, index);
            }

            index = tokens[1].lastIndexOf('*');

            if (index >= 0)
            {
                isPointer = true;
                type = tokens[1].substring(0, index);
            }
            else
            {
                type = tokens[1].substring(0);
            }


            if (size == 0)
            {
                sol.addVar("Heap." + curHeap + " false " + type + " " + isPointer + " uninitialised");
            }
            else
            {
                for (int i=0; i<size ; i++)
                {
                    sol.addVar("Heap." + curHeap + "[" + i + "]" + " false " + type + " " + isPointer + " uninitialised");
                }
            }

            curHeap ++;
            
            
            return "Heap." + (curHeap-1);
        }
        
        
        // maths expression
        String checkString;
        if (tokens[0].length() > 1)
            checkString = tokens[0].substring(1);
        else
            checkString = tokens[0];
        if (tokens.length > 2 || (!tokens[0].startsWith("\"") && !tokens[0].startsWith("'") && checkString.length() > 0 && (checkString.contains("+") || checkString.contains("-") || checkString.contains("*") || checkString.contains("/"))))
        {
            return mathExpression(source);
        }
        
        
        // return literal value
        return tokens[0];
    }
    
    private static String mathExpression(String exp)
    {
        int index = Integer.MAX_VALUE;
        char[] characters = {'+','-','*','/'};
        
        for (char c : characters)
        {
            int i2 = exp.indexOf(c);
            
            // avoid - at start otherwise get smallest
            if (i2 > 0 && i2 < index)
                index = i2;
        }
        
        String[] tokens = {exp.substring(0, index).trim(), exp.substring(index, index+1).trim(), exp.substring(index+1).trim()};
        
        String lh = getValue(tokens[0]);
        String rh = getValue(tokens[2]);
        
        
        switch (tokens[1])
        {
            case "+":
                return Double.toString(Double.parseDouble(lh) + Double.parseDouble(rh));
                
            case "-":
                return Double.toString(Double.parseDouble(lh) - Double.parseDouble(rh));
                
            case "*":
                return Double.toString(Double.parseDouble(lh) * Double.parseDouble(rh));
                
            case "/":
                return Double.toString(Double.parseDouble(lh) / Double.parseDouble(rh));
        }
        
        return "NAN";
    }
    
    private static int getFuncLine(String func)
    {
        for (int i=1; i<=prob.getNumLines(); i++)
        {
            String line = prob.getLine(i);
            
            String[] tokens = line.split(" ");
            
            if (tokens.length < 2)
                continue;
            
            String check = tokens[1];
            
            if (check.startsWith(func))
                return i;
        }
        
        return -1;
    }
    
    private static int callFunc(String call)
    {
        int index = call.indexOf('(');
            
        int funcLine = getFuncLine(call.substring(0, index));


        // get values for parameters passing using current frame context

        String[] para = call.substring(index+1, call.indexOf(')')).split(",");

        for (int i=0; i<para.length; i++)
        {
            para[i] = getValue(para[i].trim());
        }

        // create new frame
        curFrame ++;
        createScope(getCurLine());


        String funcDef = prob.getLine(funcLine);

        String[] paraNames = funcDef.substring(funcDef.indexOf('(')+1, funcDef.indexOf(')')).split(",");

        for (int i=0; i<paraNames.length; i++)
        {
            paraNames[i] = paraNames[i].trim();

            String name = paraNames[i].split(" ")[1];
            String type = paraNames[i].split(" ")[0];

            allocation(type, name);

            // maybe can fix problem by having assignment take the frame to use rather than just use curFrame
            assignment(name, para[i]);
        }

        return funcLine + 2;
    }
    
    
    private static class Scope
    {
        public final int callLine;
        
        public final LinkedList<String> varNames = new LinkedList<>();
        
        public Scope(int callLine)
        {
            this.callLine = callLine;
        }
        
    }
}
