package its;

import java.util.Collection;

/**
 *
 * @author Ryan Marker
 */
public interface MemoryContainer extends NodeGraphics
{
    public void addMemObj(MemoryObject obj);
    public boolean removeMemObj(MemoryObject obj);
    public Collection<MemoryObject> get();
    public MemoryObject get(String name);
    public void clear();
    public void setLocked(boolean l);
    public String generateTextRep();
}
