package its;

import javafx.scene.Node;

/**
 *
 * @author Ryan Marker
 */
public interface NodeGraphics 
{
    public Node getRoot();
}
