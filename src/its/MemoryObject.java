package its;

import javafx.scene.paint.Color;

/**
 *
 * @author Ryan Marker
 */
public interface MemoryObject extends NodeGraphics
{
    public void setName(String name);
    public String getName();
    public void setType(String type);
    public String getType();
    public void setFrame(int frame);
    public int getFrame();
    public String getLocation();
    public void setLocked(boolean l);
    public void setHighlight(Color colour);
    public String generateTextRep();
    public double getArrowX();
    public double getArrowY();
    public boolean isLogged();

}
