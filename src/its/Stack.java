package its;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

/**
 *
 * @author Ryan Marker
 */
public class Stack implements MemoryContainer
{
    private final ScrollPane pane = new ScrollPane();

    private final VBox vBox = new VBox();
    private final VBox framesBox = new VBox();
    
    
    private LinkedList<Frame> frames = new LinkedList<>();
    
    private boolean isLocked = false;
    
    private boolean logged;
    
    public Stack(boolean log)
    {
        logged = log;
        
        
        Label title = new Label("Stack");
        title.setAlignment(Pos.CENTER);
        title.setFont(new Font(20));
        
        vBox.setAlignment(Pos.CENTER);
        vBox.getChildren().add(title);
        
        vBox.getChildren().add(framesBox);
        
        addFrame();
        
        pane.setContent(vBox);
        pane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED); //pane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        pane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        pane.setPannable(false);
        pane.setFitToWidth(true);

        framesBox.setSpacing(10);

        
        pane.setOnMousePressed((MouseEvent me) -> {
            if (isLocked)
                return;
            
            ToggleButton selected = (ToggleButton) ITS.symbolTable.group.getSelectedToggle();
            
            if (selected == null)
                return;
              
            MemoryObject newObj = null;
            
            if (selected.getText().equalsIgnoreCase("variable"))
                newObj = new Var(false, logged);
            else if (selected.getText().equalsIgnoreCase("array"))
            {
                newObj = new Arr(logged);
            }
            
            if (newObj != null)
                addMemObj(newObj);
        });
    }
    
    public void addFrame()
    {
        // check if too many frames
        if (frames.size() > 15)
        {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Stack overflow");
            alert.setHeaderText(null);
            alert.setContentText("Too many frames have been placed on the stack for this system.");

            alert.showAndWait();
            
            return;
        }
        
        Frame newFrame = new Frame();
        newFrame.setFrame(frames.size());
        
        frames.add(newFrame);
        
        framesBox.getChildren().add(newFrame.getRoot());
        
        ITS.printLogMsg("Stack: Add frame");
    }
    

    public void removeFrame(int f)
    {
        // cannot remove last frame
        if (frames.size() <= 1)
            return;
        
        if (f >= 0)
        {
            framesBox.getChildren().remove(framesBox.getChildren().size() - frames.size() + f);
            Frame frame = frames.remove(f);
            frame.clear();
            
            ITS.printLogMsg("Stack: Remove frame " + f);
        }
    }
    
    private void removeFrame(Frame f)
    {
        // cannot remove last frame
        if (frames.size() <= 1)
            return;
        
        
        int index = frames.indexOf(f);

        if (index >= 0)
        {
            framesBox.getChildren().remove(framesBox.getChildren().size() - frames.size() + index);
            frames.remove(index);
            f.clear();
            
            ITS.printLogMsg("Stack: Remove frame " + index);
        }
    }
    
    public int getNumberFrames()
    {
        return frames.size();
    }
    
    @Override
    public void addMemObj(MemoryObject obj)
    {
        frames.getLast().addMemObj(obj);
    }
    
    /**
     * Adds a memObj to the frame of specified number. If the frame number is higher than the number of existing frames, then new frames are created.
     * Note that the number corresponding to a particular frame can change if frames are removed.
     * 
     * @param obj The object to add
     * @param frame The frame number to add the object to
     */
    public void addMemObj(MemoryObject obj, int frame)
    {
        if (frame < 0)
        {
            return;
        }
        
        while (frame >= frames.size())
            addFrame();
        
        frames.get(frame).addMemObj(obj);
    }

    @Override
    public boolean removeMemObj(MemoryObject obj)
    {
        for (Frame f : frames)
        {
            boolean success = f.removeMemObj(obj);
            
            if (success)
            {
                return true;
            }
        }
        
        return false;
    }
    
    @Override
    public Collection<MemoryObject> get()
    {
        Collection<MemoryObject> result = new LinkedList<>();
        
        for (Frame f : frames)
        {
            result.addAll(f.get());
        }
        
        return result;
    }

    @Override
    public MemoryObject get(String name)
    {
        for (Frame f : frames)
        {
            MemoryObject m = f.get(name);
            if (m != null)
                return m;
        }
        
        return null;
    }
    

    @Override
    public void clear()
    {
        while (frames.size() > 1)
        {
            removeFrame(frames.getLast());
        }
        
        frames.getFirst().clear();
    }
    
    
    @Override
    public void setLocked(boolean l)
    {
        isLocked = l;
        
        vBox.setMouseTransparent(l);
        
        for (Frame v : frames)
        {
            v.setLocked(l);
        }
    }
    
    @Override
    public String generateTextRep()
    {
        String result = "";
        
        for (Frame v : frames)
        {
            result += v.generateTextRep();
        }
        
        return result;
    }
    
    @Override
    public Node getRoot()
    {
        return pane;
    }
    
    
    private class Frame implements MemoryContainer
    {

        private int frame = 0;
        
        Separator sep = new Separator(Orientation.HORIZONTAL);
        private final Label label = new Label("Frame");
        
        private final VBox vBox = new VBox();
        
        private final ArrayList<MemoryObject> memObjects = new ArrayList<>();
        

        public Frame()
        {
            vBox.setSpacing(10);
            vBox.setPadding(new Insets(10));
            vBox.setStyle("-fx-border-style: dashed; -fx-border-color: black; -fx-border-width: 2;");
            vBox.getChildren().addAll(sep, label);
            
            sep.setPadding(new Insets(15, 0, 15, 0));
            
            sep.setOnMouseClicked((MouseEvent me) -> {
                ToggleButton sel = (ToggleButton)(ITS.symbolTable.group.getSelectedToggle());

                if (sel.getText().equalsIgnoreCase("delete"))
                {
                    removeFrame(this);
                }
            });
        }
        
        public void setFrame(int f)
        {
            label.setText("Frame" + Integer.toString(f) + ".");
            
            frame = f;
            
            for (MemoryObject o : memObjects)
            {
                o.setFrame(f);
            }
        }
        
        public int getFrame()
        {
            return frame;
        }
        
        @Override
        public void setLocked(boolean l)
        {
            getRoot().setMouseTransparent(l);
            
            for (MemoryObject v : memObjects)
            {
                v.setLocked(l);
            }
        }
        
        @Override
        public void addMemObj(MemoryObject obj)
        {
            memObjects.add(obj);
            obj.setFrame(frame);
            vBox.getChildren().add(obj.getRoot());
        }

        @Override
        public boolean removeMemObj(MemoryObject obj)
        {
            int index = memObjects.indexOf(obj);

            if (index >= 0)
            {
                vBox.getChildren().remove(vBox.getChildren().size() - memObjects.size() + index);
                memObjects.remove(index);
                return true;
            }
            
            return false;
        }

        @Override
        public MemoryObject get(String name)
        {
            for (MemoryObject m : memObjects)
            {
                if (m.getName().equals(name))
                    return m;
            }

            return null;
        }
        
        @Override
        public Collection<MemoryObject> get()
        {
            return new LinkedList<>(memObjects);
        }

        @Override
        public void clear()
        {
            while (!memObjects.isEmpty())
            {
                removeMemObj(memObjects.get(0));
            }
        }


        @Override
        public String generateTextRep()
        {
            String result = "";

            for (MemoryObject v : memObjects)
            {
                result += v.generateTextRep();
            }

            return result;
        }

        @Override
        public Node getRoot()
        {
            return vBox;
        }
        
    }
    
}
