package its;

import java.util.LinkedList;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

/**
 *
 * @author Ryan Marker
 */
public class FeedbackInterface implements NodeGraphics
{
    private final ScrollPane root = new ScrollPane();
    private final VBox vbox = new VBox();
    
    private final Text header = new Text("Feedback:");
    private final TextFlow textFlow = new TextFlow();
    private final Text feedbackText = new Text();
    
    private final String[] passedMessages = {"Excellent work :)", "Good job on passing this line :)", "Your solution is correct :)", "Well done! You’ve completed this line of the problem :)"};
    private int curPassedMsg = 0;

    public FeedbackInterface()
    {
        header.setFont(new Font(15));
        feedbackText.setFont(new Font(15));
        feedbackText.wrappingWidthProperty().bind(vbox.widthProperty());
        HBox.setHgrow(feedbackText, Priority.ALWAYS);
        
        textFlow.getChildren().add(feedbackText);
        VBox.setVgrow(textFlow, Priority.NEVER);
        
        vbox.setPadding(new Insets(20));
        vbox.setSpacing(5);
        vbox.getChildren().addAll(header, textFlow);
        HBox.setHgrow(vbox, Priority.ALWAYS);
        
        root.setContent(vbox);
        root.setFitToWidth(true);
        root.setFitToHeight(false);
        root.setPrefViewportHeight(250);
        root.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        
    }
    
    public void displayIntro()
    {
        feedbackText.setText("- This software is designed to help develop program comphrehension skills.\n"
                + "- It allows you to step through several programs and make changes to reflect what is happening within memory.\n"
                + "- You can add both variables and arrays to either the stack or the heap and fill in their properties\n"
                + "- You can also ask for feedback on your solution attempt whenever you want.\n"
                + "- There are several videos within the folder containing this tool, which help explain this tool.");
    }
    
    public void clearFeedback()
    {
        feedbackText.setText("");
    }

    public void setFeedback(LinkedList<String> feedback)
    {
        if (feedback.isEmpty())
        {
            setPassedMessage();
        }
        else
        {
            
            String msg = "";
            for (String f : feedback)
                msg += f;
            feedbackText.setText("There are " + feedback.size() + " error(s)\n\n" + msg);
        }
        
        
        ITS.printLogMsg("Feedback: " + feedbackText.getText().replaceAll("\n", ", "));
    }
    
    
    private void setPassedMessage()
    {
        int next = curPassedMsg;
        
        while (next == curPassedMsg)
        {
            next = (int)(Math.random() * passedMessages.length);
        }
        
        curPassedMsg = next;
        feedbackText.setText(passedMessages[curPassedMsg]);
    }

    @Override
    public Node getRoot()
    {
        return root;
    }
    
}
