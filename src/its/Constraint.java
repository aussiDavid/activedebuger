package its;

import static its.ITS.probInterface;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 *
 * @author Ryan Marker
 */
public class Constraint
{

    /**
     * Parses and carries out a constraint (such as a relevance or satisfaction condition) using the default binding.
     * 
     *
     * @param s The constraint to parse
     * @return The binding list produced
     */
    public static LinkedList<Binding> parseConstraint(String s)
    {
        LinkedList<Binding> binding = new LinkedList<>();
        binding.add(new Binding());
        return parseConstraint(s, binding);
    }

    /**
     * Parses and carries out a constraint (such as a relevance or satisfaction condition) using the given bindings.
     *
     * @param s The constraint to parse
     * @param binding The input bindings to use
     * @return The binding list produced
     */
    public static LinkedList<Binding> parseConstraint(String s, LinkedList<Binding> binding)
    {
        if (s.charAt(0) == '(')
        {
            int level = 1;

            // find closing )
            for (int i=1; i<s.length(); i++)
            {
                if (s.charAt(i) == '(')
                    level ++;
                else if (s.charAt(i) == ')')
                    level --;

                // found closing ) so brackets balance
                if (level == 0)
                {
                    // parse the command inside the outer ()
                    LinkedList<Binding> newBinding = parseConstraint(s.substring(1, i), binding);

                    // check if there is anther command after
                    int next = s.indexOf('(', i);

                    if (next == -1)
                    {
                        // nothing after so return
                        return newBinding;
                    }
                    else
                    {
                        // parse next command with bindings gotton as output as the new input
                        return parseConstraint(s.substring(next), newBinding);
                    }
                }
            }
        }
        else
        {
            // Did not start with ( so much be an actual command

            // see what command it is and do it
            if (s.startsWith("match"))
            {
                return Pattern.match(s.substring(5).trim(), binding);
            }
            else if (s.startsWith("testSymbol"))
            {
                return Pattern.testSymbol(s.substring(10).trim(), binding);
            }
            else if (s.startsWith("test")) // after testSymbol so correct one is picked
            {
                return Pattern.test(s.substring(4).trim(), binding);
            }
            else if (s.startsWith("and"))
            {
                int start = s.indexOf('(');
                int end = findCloseBracket(s);

                // get first list
                LinkedList<Binding> b1 = parseConstraint(s.substring(start+1, end).trim(), binding);

                start = s.indexOf('(', end);
                end = findCloseBracket(s.substring(start));

                // get second list
                LinkedList<Binding> b2 = parseConstraint(s.substring(start+1, start+end).trim(), binding);

                return Pattern.and(b1, b2);
            }
            else if (s.startsWith("or"))
            {
                int start = s.indexOf('(');
                int end = findCloseBracket(s);

                // get first list
                LinkedList<Binding> b1 = parseConstraint(s.substring(start+1, end).trim(), binding);

                start = s.indexOf('(', end);
                end = findCloseBracket(s.substring(start));

                // get second list
                LinkedList<Binding> b2 = parseConstraint(s.substring(start+1, start+end).trim(), binding); // need start+end as sent substring to findCloseBracket

                return Pattern.or(b1, b2);
            }
            else if (s.startsWith("not"))
            {
                int start = s.indexOf('(');
                int end = findCloseBracket(s);

                // get list
                LinkedList<Binding> b1 = parseConstraint(s.substring(start+1, end).trim(), binding);

                return Pattern.not(b1);
            }
            else if (s.startsWith("split"))
            {
                return Pattern.split(s.substring(5).trim(), binding);
            }
        }

        // if reach here then something went wrong
        System.err.println("Error in parseConstraint s: " + s + ", binding: " + binding);

        return null;
    }

    /**
     * Finds the end of the first outer ( ) pair.
     *
     * @param s The string to search
     * @return The index of the outer closing ). -1 if there is an issue.
     */
    public static int findCloseBracket(String s)
    {
        int startIndex = s.indexOf('(');

        int level = 1;

        for (int i=startIndex +1; i<s.length(); i++)
        {
            if (s.charAt(i) == '(')
                level ++;
            else if (s.charAt(i) == ')')
                level --;

            if (level == 0)
            {
                return i;
            }
        }

        return -1;
    }

    /**
     * Gets Constraint feedback, gives differing detail levels of feedback depending on the feedbackLevel
     * @param feedbackLevel
     * @return
     */
    public String getMultilevelFeedback(Binding bindingToGetFeedbackFor)
    {
    	int feedbackLevel = bindingToGetFeedbackFor.timesSeenAlready-1;
    	switch(feedbackLevel)
    	{
    	case 0:			//Least detailed level of feedback - general 'error detected on line' message
    		return "Error detected";
    	case 1:			//More detailed but not giving specifics feedback
    		return generalFeedback;
    	case 2:			//Gives specifics on what is erroneous
    		return getFeedback(bindingToGetFeedbackFor);
    	}
    	return "Exception reached";
    }
    
    
    private int id;

    private String generalFeedback; //General feedback
    private String feedback;	//Detailed feedback
    public String relevance;
    public String satisfaction;

    public Constraint(String s)
    {
        String[] sp = s.split(",");
        id = Integer.parseInt(sp[0].trim());
        relevance = sp[1].trim();
        satisfaction = sp[2].trim();
        feedback = sp[3].trim();
        generalFeedback = feedback.replace("?n", "Something");
    }


    /**
        ss - student solution
        is - ideal solution
    */
    public LinkedList<Binding> check(Solution ss, Solution is)
    {
        LinkedList<Binding> result = checkRelevance();


        if (result.isEmpty())
        {	//Conjecture: if result is empty, constraint not relevant
        	return result;
        }
            


        result = checkSatisfaction(result);


        ITS.addStudentModel(this, probInterface.getProblemNum(), CInterpreter.getCurLine(), result.isEmpty());
        if(result.isEmpty()==false)
        {
        	System.out.println("Error on " + CInterpreter.getCurLine());
        }

        return result;
    }

    /**
     * Parses the relevance constraint with the default binding and removes any output bindings which are false.
     *
     * @return The binding list produced by the relevance condition.
     */
    public LinkedList<Binding> checkRelevance()
    {
        LinkedList<Binding> relBind = parseConstraint(relevance);


        for (ListIterator<Binding> it = relBind.listIterator(); it.hasNext();)
        {
            Binding b = it.next();

            if (!b.isSuccess())
            {
                it.remove();
            }
        }
        //System.out.println("Relevance: " + relBind.size());
        return relBind;
    }

    /**
     * Parses the satisfaction constraint with the provided input binding and removes any output bindings which are true.
     *
     * @param bindings The input bindings to be used when parsing
     * @return The output bindings
     */
    public LinkedList<Binding> checkSatisfaction(LinkedList<Binding> bindings)
    {
        LinkedList<Binding> satBind = parseConstraint(satisfaction, bindings);


        for (ListIterator<Binding> it = satBind.listIterator(); it.hasNext();)
        {
            Binding b = it.next();

            if (b.isSuccess())
            {
                it.remove();
            }
            else
            {
            	//System.out.println("Failed a binding");
            	
            }
        }

        return satBind;

    }

    public LinkedList<String> getFeedback(LinkedList<Binding> bindings)
    {
        LinkedList<String> result = new LinkedList<>();

        bindings.stream().forEach((b) -> {
            result.add(getFeedback(b));
        });

        return result;
    }

    /**
     * Generate feedback for this constraint which is based on the corresponding constraint.
     *
     * @param b
     * @return
     */
    public String getFeedback(Binding b)
    {
        String result = feedback;

        int start, end;

        start = result.indexOf('?');

        while (start >= 0)
        {
            end = result.indexOf(' ', start);

            if (end > result.length() || end < 0)
                end = result.length();

            String name = result.substring(start + 1, end);

            if (b.isDefined(name))
            {
                if (end == result.length())
                    result = result.substring(0, start) + b.getValue(name);
                else
                    result = result.substring(0, start) + b.getValue(name) + result.substring(end);
            }
            else
            {
                if (end == result.length())
                    result = result.substring(0, start) + "undefined var: " + name;
                else
                    result = result.substring(0, start) + "undefined var: " + name + result.substring(end);
            }


            start = result.indexOf('?');
        }

        return result;
    }

    public int getId()
    {
        return id;
    }


    @Override
    public String toString()
    {
        return "ID: " + id + ", Relevance: " + relevance + ", Satisfaction: " + satisfaction + ", Feedback: " + feedback;
    }

}
