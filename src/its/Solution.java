package its;

import java.util.LinkedList;

/**
 *
 * @author Ryan Marker
 */
public class Solution
{
    public LinkedList<String> variables = new LinkedList<>();
    
    public Solution()
    {
        
    }
    
    public Solution(String s)
    {
    	System.out.println(s);
        String copy;
        
        
        copy = s.replaceFirst("Var: ", " ");
        
        String[] var = copy.split("@");
        
        for (int i=0; i<var.length; i++)
        {
            String entry = var[i].trim();
            
            if (entry.isEmpty())
                continue;
                
            Solution.this.addVar(entry);
        }
            
    }
    
    public void addVar(String v)
    {
        variables.add(v);
    }
    
    public boolean removeVar(String name)
    {
        for (int j=0; j<variables.size(); j++)
        {
            String v = variables.get(j);
            
            String[] elements = v.split(" ");
            
            if (elements[0].equals(name))
            {
                variables.remove(j);
                
                return true;
            }
        }
        
        return false;
    }
    
    public String getVar(String name)
    {
        for (String v : variables)
        {
            if (v.split(" ")[0].equals(name))
                return v;
        }
        
        return null;
    }
    
    public LinkedList<String> getAllVar()
    {
        return variables;
    }
    
    public boolean editVar(String name, int index, String newValue)
    {
        
        
        for (int j=0; j<variables.size(); j++)
        {
            String v = variables.get(j);
            
            String[] elements = v.split(" ");
            
            if (elements[0].equals(name))
            {
                // if setting value
                if (index == 4)
                {
                    // if not pointer
                    if (elements[3].equalsIgnoreCase("false"))
                    {
                        // if type is int
                        if (elements[2].equalsIgnoreCase("int"))
                        {
                            // remove fractional part
                            if (newValue.indexOf('.') > 0)
                                newValue = newValue.substring(0, newValue.indexOf('.'));
                        }

                    }

                    
                }
                
                String newVar = "";
                
                int i = 0;
                
                for (String e : elements)
                {
                    if (index == i)
                    {
                        newVar += newValue + " ";
                    }
                    else
                    {
                        newVar += e + " ";
                    }
                    
                    i++;
                }
                
                variables.set(j, newVar);
                
                return true;
            }
        }
        
        return false;
    }

    @Override
    public String toString()
    {
        return toString("var");
        
    }
    
    public String toString(String section)
    {
        section = section.trim();
        
        if (section.equalsIgnoreCase("var"))
        {
            String result = "Var: @";
        
            for (String v : variables)
            {
                result += " " + v + " @";
            }

            return result;
        }
        
        return null;
    }
}
