package its;

import java.util.LinkedList;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;

/**
 *
 * @author Ryan Marker
 */
public class Var implements MemoryObject
{
    private static Var pressedVar = null;
    
    private boolean logged = false;
    
    private final HBox hBox = new HBox();
    
    private final TextField location = new TextField();
    
    private final TextField name = new TextField();
    private final TextField type = new TextField();
    private final TextField value = new TextField();
    
    private final ChoiceBox<String> isPointer = new ChoiceBox<>();
    
    private final Separator sep = new Separator(Orientation.VERTICAL);
    
    private int frame;
    
    private final Color c1 = new Color(0, 0, 1, 1);
    private final Color c2 = new Color(0, 0.35, 1, 1);
    private final Color c3 = new Color(0, 0.7, 1, 1);
    
    private final String NOTPTR = "Not pointer";
    private final String PTR = "* (Pointer)";

    public Var(boolean locked, boolean log) 
    {  
        hBox.setPadding(new Insets(10));
        hBox.setSpacing(5);
        hBox.setStyle("-fx-border-style: solid; -fx-border-color: black; -fx-border-width: 2;");
        
        location.setPrefColumnCount(10);
        location.setEditable(false);
        location.setBackground(Background.EMPTY);
        location.setMouseTransparent(true);
        
        name.setText("name");
        name.setTooltip(new Tooltip("name"));
        
        type.setText("type");
        type.setTooltip(new Tooltip("type"));
        
        
        isPointer.getItems().addAll(NOTPTR, PTR);
        isPointer.setValue(NOTPTR);

        
        if (locked)
        {
            value.setText("value");
            
            setLocked(true);
        }
        else
        {
            value.setText("uninitialised");
        }
        
        value.setTooltip(new Tooltip("value"));
        
        
        sep.setMouseTransparent(true);
        sep.setPadding(new Insets(0, 20, 0, 0));
        
        HBox.setHgrow(hBox, Priority.ALWAYS);
        
        hBox.getChildren().addAll(name, type, isPointer, sep, value);
        
        
        hBox.setOnMouseClicked((MouseEvent me) -> {
            if (locked)
                return;
            
            ToggleButton selected = (ToggleButton) (ITS.symbolTable.group.getSelectedToggle());
            
            if (selected != null)
            {
                if (selected.getText().equalsIgnoreCase("delete"))
                {
                    // one will fail, unless same object added to both, fail should have no bad consequences
                    ITS.stack.removeMemObj(this);
                    ITS.heap.removeMemObj(this);
                    
                    ITS.printLogMsg("Var: Delete " + this.generateTextRep());
                }
                
                if (selected.getText().equalsIgnoreCase("Pointer"))
                {
                    MouseButton button = me.getButton();
                    
                    
                    if (button == MouseButton.SECONDARY)
                        pressedVar = this;
                    else if (button == MouseButton.PRIMARY && pressedVar != null)
                    {
                        setValue(pressedVar.getLocation() + pressedVar.getName());
                    }
                }
                
            }
            
        });
        
        
        hBox.setOnMouseEntered((MouseEvent me) -> 
        {
            // pointer highlighting
            
            LinkedList<Var> var = new LinkedList<>();
            
            for (MemoryObject m : ITS.stack.get())
            {
                if (m instanceof Var)
                {
                   var.add((Var)m);
                }
                else if (m instanceof Arr)
                {
                    var.addAll((((Arr)(m)).variables));
                }
                
            }
            
            for (MemoryObject m : ITS.heap.get())
            {
                if (m instanceof Var)
                {
                   var.add((Var)m);
                }
                else if (m instanceof Arr)
                {
                    var.addAll((((Arr)(m)).variables));
                }
                
            }
            
            
            ITS.ram.clearArrows();
            
            for (Var v : var)
            {
                // maybe change so get generate tex rep of v and then use that
                
                if (v == this)
                {
                    v.setHighlight(c2);
                }
                else if (getValue().contains(v.getLocation() + v.getName())) 
                {
                    ITS.ram.addArrow(this, v);
                    v.setHighlight(c1);
                }
                else if (v.getValue().equals(getLocation() + getName()))
                {
                    v.setHighlight(c3);
                }
                else
                {
                    v.setHighlight(null);
                }
            }
            
        });
        
        hBox.setOnMouseExited((MouseEvent me) -> 
        {
            ITS.ram.clearArrows();
        });
        
        if (log)
        {
            ITS.printLogMsg("Var: Create Var: uninitialised");
            logged = true;
        }
    }
    
    public Var(String s, boolean log)
    {
        this(false, false);
        
        String[] components = s.split(" ");
        
        name.setText(components[0]);
        type.setText(components[2]);
        setPointer(Boolean.parseBoolean(components[3]));
        value.setText(components[4]);
        
        // make sure log is after name, etc are set
        if (log)
        {
            ITS.printLogMsg("Var: Create Var: " + toString());
            logged = true;
        }
    }
    
    @Override
    public boolean isLogged()
    {
        return logged;
    }
    
    
    @Override
    public void setLocked(boolean l)
    {
        isPointer.setDisable(l);
        
        l = !l;
        
        name.setEditable(l);
        type.setEditable(l);
        value.setEditable(l);
    }
    
    /**
     * Sets the editable status of a component of the variable.
     * 
     * @param field The component to affect. 1 - name, 2 - type, 3 - value, 4 - pointer
     * @param l True for locked, false for editable
     */
    public void setLocked(int field, boolean l)
    {
        l = !l;
        
        switch (field)
        {
            case 1:
               name.setEditable(l);
                break;
                
            case 2:
                type.setEditable(l);
                break;
                
            case 3:
                value.setEditable(l);
                break;
                
            case 4:
                isPointer.setDisable(!l);
        }
        
        
        
        
    }
    
    @Override
    public void setName(String n)
    {
        if (isLogged())
            ITS.printLogMsg("Var: Rename " + getLocation() + name.getText() + " to " + n);
        
        name.setText(n);
    }
    
    @Override
    public String getName()
    {
        return name.getText();
    }
    
    @Override
    public void setType(String n)
    {
        if (isLogged())
            ITS.printLogMsg("Var: Change type of " + getLocation() + name.getText() + " from " + getType() + " to " + n);
        
        type.setText(n);
    }
    
    @Override
    public String getType()
    {
        return type.getText();
    }
    
    public void setValue(String n)
    {
        if (isLogged())
            ITS.printLogMsg("Var: Change value of " + getLocation() + name.getText() + " from " + getValue()+ " to " + n);
        
        value.setText(n);
    }
    
    public String getValue()
    {
        return value.getText();
    }
    
    public void setPointer(boolean value)
    {
        if (isLogged())
            ITS.printLogMsg("Var: Change pointer of " + getLocation() + name.getText() + " from " + isPointer() + " to " + value);
        
        
        if (value)
            isPointer.setValue(PTR);
        else
            isPointer.setValue(NOTPTR);
    }
    
    public Boolean isPointer()
    {
        return isPointer.getValue().equals(PTR);
    }
    
    @Override
    public void setFrame(int frame)
    {
        this.frame = frame;
        
        if (frame < 0)
        {
            location.setText("Heap.");
        }
        else
        {
           location.setText("Frame" + frame + "."); 
        }
        
    }

    @Override
    public int getFrame()
    {
        return frame;
    }

    @Override
    public String getLocation()
    {
        return location.getText();
    }
    
    
    @Override
    public void setHighlight(Color colour)
    {
        
        Node node;
        /*
        if (getRoot().getParent() instanceof MemoryObject)
        {
            node = getRoot().getParent();
        }
        else*/
        {
            node = getRoot();
        }
        
        
        if (colour == null)
        {
            node.setEffect(null);
        }
        else
        {
            node.setEffect(new DropShadow(20, colour));
        }
        
    }
    
    @Override
    public String generateTextRep()
    {
        String result = "";
        
        result += " " + location.getText() + getName() + ((getFrame() == -1) ? " false " : " true ") + getType() + " " + isPointer() + " " + getValue() + " @";
        
        return result;
    }

    @Override
    public double getArrowX()
    {
        if (location.getText().startsWith("Heap"))
        {
            return 0;
        }
        else
        {
            return hBox.getWidth();
        }
    }

    @Override
    public double getArrowY()
    {
        return getRoot().getLayoutBounds().getHeight()/2;
    }
    
    
    @Override
    public Node getRoot() 
    {
        return hBox;
    }

    @Override
    public String toString()
    {
        return generateTextRep();
    }

}
