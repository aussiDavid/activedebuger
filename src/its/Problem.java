package its;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ryan Marker
 */
public class Problem
{
    private ArrayList<String> lines = new ArrayList<>();
    private ArrayList<Boolean> passed = new ArrayList<>();
    private ArrayList<Boolean> roadBlock = new ArrayList<>();

    public Problem(String filename)
    {
        try (BufferedReader reader = new BufferedReader(new FileReader(filename)))
        {
            String line;
            
            while ( (line = reader.readLine()) != null)
            {
                line = line.trim();
                
                if (line.isEmpty())
                {
                    passed.add(Boolean.TRUE);
                }
                else
                    passed.add(Boolean.FALSE);
                
                if (line.endsWith("#"))
                {
                    roadBlock.add(Boolean.TRUE);
                }
                else
                    roadBlock.add(Boolean.FALSE);
                
                lines.add(line);
            }
                
        }
        catch (FileNotFoundException ex) 
        {
            Logger.getLogger(Problem.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (IOException ex)
        {
            System.err.println("IOException: " + ex.toString());
        }
        
        // make so last line is always blank
        lines.add("");
        passed.add(Boolean.FALSE);
        roadBlock.add(Boolean.FALSE);
    }
    
    public void setPassedLine(int line, boolean value)
    {
        if (line > lines.size())
            line = lines.size();
        
        passed.set(line-1, value);
    }
    
    public boolean hasPassedLine(int line)
    {
        if (line > lines.size())
            line = lines.size();
        
        return passed.get(line-1);
    }
    
    public boolean isRoadBlock(int line)
    {
        if (line > lines.size())
            line = lines.size();
        
        return roadBlock.get(line-1);
    }
    
    public String getLine(int line)
    {
        if (line > lines.size())
            line = lines.size();
        
        return lines.get(line-1);
    }
    
    public int getNumLines()
    {
        return lines.size();
    }
    
    /**
     * Finds the matching brace of a {} pair. This means if a new {} is found inside the specified brace than it should correctly be ignored.
     * 
     * @param startLine The line number with one of { or }
     * @param step Which brace it is searching for. 1 for finding } by going forward or -1 for finding { by going backwards
     * @return The line number of the other brace in the pair
     */
    public int findBracket(int startLine, int step)
    {
        int balance = step;
        
        int i = startLine + step;
        
        while (balance != 0)
        {
            String checkLine = getLine(i);
            
            if (checkLine.startsWith("{"))
                balance ++;
            else if (checkLine.startsWith("}"))
                balance --;
            
            i += step;
        }
        
        return i - step;
    }

    @Override
    public String toString()
    {
        String result = "";
        
        for (String s : lines)
        {
            result += s + "\n";
        }
        
        return result;
    }
    
    
}
